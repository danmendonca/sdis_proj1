package message;

import java.net.DatagramPacket;

/**
 * Java class PeerMsg
 *
 */
public class PeerMsg implements Runnable {

    public enum messageType {

        PUTCHUNK, STORED, GETCHUNK, CHUNK, DELETE, REMOVED
    };

    public enum connectionType {

        SEND, RECEIVE;
    }

    protected byte rawData[];

    protected connectionType connectMsg;
    protected messageType typeMsg;
    protected String versionMsg;
    protected String fileIdMsg;

    /**
     * PeerMsg Class Construtor
     *
     */
    public PeerMsg() {
    }

    /**
     * Factory Method
     *
     * @param receivedPacket
     * @return
     */
    public static PeerMsg getInstance(DatagramPacket receivedPacket, int bytesRead) {

        String strHeader = new String(receivedPacket.getData());
        String dataArray[] = strHeader.split("\\r\\n\\r\\n");
        String msgHeader[] = dataArray[0].split("\\s+");

        switch (msgHeader[0].toUpperCase()) {

            case "PUTCHUNK":
                return new PutMsg(receivedPacket.getData(), bytesRead);

            case "STORED":
                return new StoredMsg(receivedPacket);

            case "GETCHUNK":
                return new GetMsg(receivedPacket.getData());

            case "CHUNK":
                return new ChunkMsg(receivedPacket.getData(), bytesRead);

            case "DELETE":
                return new DelMsg(receivedPacket.getData());

            case "REMOVED":
                return new RemMsg(receivedPacket);

            default:
                throw new IllegalArgumentException("Invalid message type");
        }
    }

    /**
     *
     */
    @Override
    public void run() {
    }
    
    /**
     * Returns message connection type
     * @return 
     */
    public connectionType getConnectType(){
        return connectMsg;
    }

    /**
     * Returns message type
     *
     * @return messageType
     */
    public messageType getTypeMsg() {
        return typeMsg;
    }

    /**
     * Returns message protocol version
     *
     * @return protocol version
     */
    public String getVerMsg() {
        return versionMsg;
    }

    /**
     * Returns message File ID
     *
     * @return File ID
     */
    public String getFileIdMsg() {
        return fileIdMsg;
    }
    
    /**
     * Returns message Chunk Number
     *
     * @return Chunk Number
     */
    public String getChunkNoMsg() {
        return null;
    }
    
    /**
     * Returns message replication degree
     *
     * @return replication degree
     */
    public String getRepDegMsg() {
        return null;
    }
    
    /**
     * Returns message body with chunk data
     *
     * @return body with chunk data
     */
    public byte[] getBodyMsg() {
        return null;
    }

    /**
     * Returns message header
     *
     * @return message header
     */
    protected String[] parseHeader() {
        String strHeader = new String(this.rawData);
        String dataArray[] = strHeader.split("\\r\\n\\r\\n");
        String msgHeader[] = dataArray[0].split("\\s+");
        return msgHeader;
    }

    /**
     * Parses and validates protocol version field
     *
     * @param mVer
     */
    protected void parseVersion(String mVer) {
        if ((mVer.length() != 3) || (mVer.indexOf(".") != 1)) {
            throw new IllegalArgumentException("Invalid version field");
        }

        Integer.parseInt(mVer.charAt(0) + "");
        Integer.parseInt(mVer.charAt(2) + "");
    }

    /**
     * Parses and validates File ID field
     *
     * @param mfileId
     */
    protected void parseFileId(String mfileId) {
        if (mfileId.length() != 64) {
            throw new IllegalArgumentException("Invalid file ID field");
        }
    }

    /**
     * Parses and validates Chunk Number field
     *
     * @param mChunk
     */
    protected void parseChunkNo(String mChunk) {
        if ((mChunk.length() > 6) && (mChunk.length() < 1)) {
            throw new IllegalArgumentException("Invalid chunk number field");
        }
        Integer.parseInt(mChunk);
    }

    /**
     * Returns string with message fields
     *
     * @return message fields
     */
    @Override
    public String toString() {

        StringBuilder strMsg = new StringBuilder();

        //Type
        strMsg.append("Type: ");
        if (this.typeMsg != null) {
            strMsg.append(this.typeMsg);
        }
        strMsg.append(System.lineSeparator());

        //Version
        strMsg.append("Version: ");
        if (this.versionMsg != null) {
            strMsg.append(this.versionMsg);
        }
        strMsg.append(System.lineSeparator());

        //File ID
        strMsg.append("File ID: ");
        if (this.fileIdMsg != null) {
            strMsg.append(this.fileIdMsg);
        }
        strMsg.append(System.lineSeparator());

        return strMsg.toString();
    }

}
