/**
 *
 */
package general;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

import message.DelMsg;
import message.GetMsg;
import message.PutMsg;
import message.RemMsg;

public class Peer {

	public final static boolean DEBUG = true;

	private static Peer peer = null;

	//MDB
	private MDBWrapper mdb = null;
	private String mdbIP;
	private int mdbPort;

	//MCW
	private MCWrapper mc = null;
	private String mcIP;
	private int mcPort;

	//MDR
	private MDRWrapper mdr = null;
	private String mdrIP;
	private int mdrPort;

	private MulticastSocket senderSocket = null;

	private InitiatorPeerFile peerFile;
	private Chunk chunk;

	final public static int MIN_BACKUP_WAIT_TIME = 500;
	final public static int MAX_BACKUP_WAIT_TIME = 8000;

        /**
         * 
         * @param args 
         */
	public static void main(String args[]) {

		DBInteraction.getInstance();
		
		//MC IP and Port
		String mcIp;
		int mcPort;
		//MDB IP and Port
		String mdbIp;
		int mdbPort;
		//MDR IP and Port
		String mdrIp;
		int mdrPort;
                
                //TODO Delete after testing
                //MC IP and Port
		//mcIp = "224.0.0.3";
		//mcPort = 6000;
		//MDB IP and Port
		//mdbIp = "224.0.0.4";
		//mdbPort = 6001;
		//MDR IP and Port
		//mdrIp = "224.0.0.5";
		//mdrPort = 6002;
                
                //TODO parse args[]
                
                try {
                    if (args.length != 6) {
                        throw new IllegalArgumentException("Invalid number of arguments.");
                    }
                    
                    //MC 
                    mcIp = args[0];
                    mcPort = Integer.parseInt(args[1]);
                    
                    //MDB
                    mdbIp = args[2];
                    mdbPort = Integer.parseInt(args[3]);
                    
                    //MDR
                    mdrIp = args[4];
                    mdrPort = Integer.parseInt(args[5]);
                    
                } 
                catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                    System.out.println("Usage: sdis_proj_1 <MC-IP> <MC-Port> <MDB-IP> <MDB-Port> <MDR-IP> <MDR-Port>");
                    return;
                }
                
                
		//Create chunks and restore directories
		File chunksDir = new File(Chunk.CHUNKS_PATH);
		File restoreDir = new File(Chunk.CHUNKS_RESTORE_PATH);
		chunksDir.mkdir();
		restoreDir.mkdir();

		//Initialize Peer
		Peer.getInstace().initPeer(mcIp, mcPort, mdbIp, mdbPort, mdrIp, mdrPort);

		//Program commands message
		System.out.println("### PEER - Protocols - Usage ###");
		System.out.println("backup  <file path> <replication degree>");
		System.out.println("restore <file path>");
		System.out.println("delete  <file Path>");
		System.out.println("reclaim");
		System.out.println("exit");

		//Get commands from System.in
		Scanner s;
		s = new Scanner(System.in);

		Boolean runInput = true;

		while (runInput) {
			System.out.println(System.lineSeparator() + "Enter command:");
			String protocol = s.next();
			String filePath;
			
			switch (protocol) {
			case "backup":
				System.out.print("Backup Protocol - File: ");
				//TODO Call backup protocol
				filePath = s.next();
				System.out.println(filePath);
				Peer.getInstace().backupFile(filePath, askDrd(s));
				break;
			case "restore":
				System.out.print("Restore Protocol - File: ");
				filePath = s.next();
				System.out.println(filePath);
				//TODO Call restore protocol
				Peer.getInstace().restoreFile(filePath);
				break;
			case "delete":
				System.out.println("File deletion Protocol - File: ");
				//TODO Call file deletion protocol
				filePath = s.next();
				System.out.println(filePath);
				Peer.getInstace().DeleteFile(filePath);
				break;
			case "reclaim":
				System.out.println("Space reclaiming Protocol");
				Peer.getInstace().reclaimSpace();
				break;
			case "exit":
				System.out.println("Exiting Peer");
				runInput = false;

				//TODO create and call exitPeer()
				Peer.getInstace().exitPeer();

				break;
			default:
			}

		}
		s.close();
	}

	/**
	 * 
	 * @param s scanner to read the input
	 * @return the desired rd that is belongs to [1, 9]
	 */
	private static int askDrd(Scanner s){
		int drd = Integer.MAX_VALUE;
		do{
			if(drd != Integer.MAX_VALUE)
				System.out.println(" 0 < Desired replication degree < 10");
			
			System.out.print("Desired replication degree: ");
			drd = s.nextInt();
			System.out.println(drd);
		}
		while(drd < 1 || drd > 10);
		return drd;
	}
	
	/**
	 * creates an instance (SINGLETON pattern) that is the basis of the project
	 *
	 * @param mcIp control channel address
	 * @param mcPort control channel port
	 * @param mdbIp backup channel address
	 * @param mdbPort backup channel port
	 * @param mdrIp restore channel address
	 * @param mdrPort restore port address
	 *
	 */
	private void initPeer(String mcIp, int mcPort, String mdbIp, int mdbPort, String mdrIp, int mdrPort) {

		try {
			//MDB
			this.mdbIP = mdbIp;
			this.mdbPort = mdbPort;
			mdb = new MDBWrapper(mdbIp, mdbPort);

			//MC
			this.mcIP = mcIp;
			this.mcPort = mcPort;
			mc = new MCWrapper(mcIp, mcPort);

			//MDR
			this.mdrIP = mdrIp;
			this.mdrPort = mdrPort;
			mdr = new MDRWrapper(mdrIp, mdrPort);

			Thread mdbThread = new Thread(mdb);
			Thread mcThread = new Thread(mc);
			Thread mdrThread = new Thread(mdr);
			mdbThread.start();
			mcThread.start();
			mdrThread.start();

			senderSocket = new MulticastSocket();
			senderSocket.setTimeToLive(1);

		} catch (IOException e) {
			System.err.println("Peer - initPeer: " + e.getMessage());
		}

		if (mdb == null || mc == null || mdr == null) {
			System.err.println("Error");
			return;
		}

		//and now, do stuff
	}

	public static Peer getInstace() {
		if (peer == null) {
			return peer = new Peer();
		}
		return peer;
	}

	/**
	 * @return the peerFile
	 */
	public InitiatorPeerFile getPeerFile() {
		return peerFile;
	}

	/**
	 * @return the chunk
	 */
	public Chunk getChunk() {
		return chunk;
	}

	/**
	 * @param peerFile the peerFile to set
	 */
	public void setPeerFile(InitiatorPeerFile newPeerFile) {
		peerFile = newPeerFile;
	}

	/**
	 * @param chunk the chunk to set
	 */
	public void setChunk(Chunk newChunk) {
		chunk = newChunk;
	}

	/**
	 *
	 * @return address of mc multicast
	 * @throws java.net.UnknownHostException
	 */
	public InetAddress getMcInet() throws UnknownHostException {   
		return InetAddress.getByName(this.mcIP);
	}

	/**
	 *
	 * @return port of the mc multicast
	 */
	public int getMcPort() {
		return this.mcPort;
	}

	/**
	 *
	 * @return address of the mdb multicast
	 * @throws java.net.UnknownHostException
	 */
	public InetAddress getMdbInet() throws UnknownHostException {
		return InetAddress.getByName(this.mdbIP);
	}

	/**
	 *
	 * @return port of the mdb multicast
	 */
	public int getMdbPort() {
		return this.mdbPort;
	}

	/**
	 *
	 * @return address of the mdb multicast
	 * @throws java.net.UnknownHostException
	 */
	public InetAddress getMdrInet() throws UnknownHostException {
		return InetAddress.getByName(this.mdrIP);
	}

	/**
	 *
	 * @return port of the mdb multicast
	 */
	public int getMdrPort() {
		return this.mdrPort;
	}

	/**
	 * Returns the MDR channel
	 *
	 * @return
	 */
	public MDRWrapper getMDR() {
		return mdr;
	}

	/**
	 * Returns the MDB channel
	 *
	 * @return
	 */
	public MDBWrapper getMDB() {
		return mdb;
	}

	/**
	 * Returns the MC channel
	 * @return 
	 */
	public MCWrapper getMC(){
		return mc;
	}

	/**
	 *
	 * @param packet to be sent through the MulticastSocket
	 */
	public void sendMsgs(DatagramPacket packet) {
		try {
			senderSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Peer - sendMsgs: " + e.getMessage());
		}
	}

	/**
	 * 
	 * @return the sender socket port
	 */
	public int getSenderPort(){
		return senderSocket.getLocalPort();
	}
	
	/**
	 * 
	 * @return the senderSocket local address
	 */
	public InetAddress getSenderAddress(){
		return senderSocket.getLocalAddress();
	}
	
	/**
	 * Close all resources for safely exiting peer
	 */
	public void exitPeer(){

		//Close all channels
		mc.closeChannel();
		mdb.closeChannel();
		mdr.closeChannel();
	}

	/**
	 * splits a file given by its path into chunks and
	 * initiates PUTCHUNK protocol
	 * @param filePath path to the file to backup
	 * @param desiredRD number of peers that should store each chunk
	 */
	protected void backupFile(String filePath, int desiredRD){
		try {
			InitiatorPeerFile backUp = new InitiatorPeerFile(filePath, desiredRD);
			backUp.saveInitiatorPeerFile();

			for(int chunkNo =1; chunkNo <= backUp.getNrChunks(); chunkNo++){

				int time = MIN_BACKUP_WAIT_TIME;
				//special FileChunk, this has data[] to work as regular chunk too!
				Chunk fileChunk = backUp.createChunk(chunkNo);
                              
				
				fileChunk.saveFileChunk(); 
				do{
					//TODO version
					PutMsg putMsg = new PutMsg("1.0", fileChunk);
					
					putMsg.run();

					Thread.sleep(time);

					if(desiredRD <= fileChunk.getFileChunkDegree())
						break;
					
					time *= 2;
				}
				while(time <= MAX_BACKUP_WAIT_TIME);

			}


		} catch (Exception e) {
			System.err.println("Peer - backupFile: " + e.getMessage());
		}

	}

	/**
	 * deletes all entry related to the file in the database
	 * and initiates DELETE protocol so other peers can do it too
	 * @param filePath path to the file to delete
	 */
	protected void DeleteFile(String filePath){
		//TODO test it

			DBInteraction dbi = DBInteraction.getInstance();
			InitiatorPeerFile ipf = dbi.getFile(filePath);

			if(ipf== null){
				System.err.println("No such file");
				return;
			}
			//TODO control version
			DelMsg delMsg = new DelMsg("1.0", ipf.getFileId());
			delMsg.run();

			ipf.delete();

	}

	/**
	 * Reclaims disk space by deleting a copy of a chunk it has backed up
	 */
	public void reclaimSpace() {

		//Analyze which chunk should be deleted
		DBInteraction dbi =DBInteraction.getInstance();
		ArrayList<Chunk> storedChunks = dbi.getAllChunks();
		Chunk delChunk = null;
		int higherRep = 0;

		if (storedChunks.isEmpty()) {
			System.out.println("No stored chunks in our disk.");
			return;
		}

		//Get chunk with higher difference between actual and desired replication degree
		for (Chunk ch : storedChunks) {
			
			//getting chunkDegree now has some db costs
			int chunkRD =ch.getChunkDegree();
			if (chunkRD > ch.getDesiredRD()) {
				int diffRep = chunkRD - ch.getDesiredRD();

				if (diffRep > higherRep) {
					higherRep = diffRep;
					delChunk = ch;
				}
			}
		}

		//If no chunk has a positive difference, choose the chunk with the higher replication degree
		if (delChunk == null) {
			higherRep = 0;
			for (Chunk ch : storedChunks) {
				int chunkRD =ch.getChunkDegree();
				if (chunkRD > higherRep) {
					higherRep = chunkRD;
					delChunk = ch;
				}
			}
		}

		//Delete chunk from disk
		delChunk.removeFromDisk();
		
		//Delete chunk from database
                delChunk.removeChunkFromDb();

		//Send REMOVED in the MC channel
		//TODO Review protocol version
		RemMsg rMsg = new RemMsg("1.0", delChunk);
		rMsg.run();

		System.out.println("Removed from disk - Chunk : ");
		System.out.println(delChunk.toString());

	}

	/**
	 * Restores previous backuped file
	 *
	 * @param filePath
	 */
	public void restoreFile(String filePath) {

		//Verify if file exists in the database
		InitiatorPeerFile initFile = DBInteraction.getInstance().getFile(filePath);

		if (initFile == null) {
			System.out.println("File doesn't exist or wasn't previously backuped in the database.");
			return;
		}

		try {
			//Send GETCHUNKS messages for which file chunk
			for (int i = 1; i <= initFile.getNrChunks(); i++) {

				File file = new File(Chunk.CHUNKS_RESTORE_PATH + i + ".data");
				Chunk fileCh = new Chunk(initFile.getFileId(), i);
                                Boolean runSendGetMsg = true;
                                int tryNum = 0;
                                int waitTime = MIN_BACKUP_WAIT_TIME;

				do {
                                        if(tryNum == 3){
                                            throw new IllegalArgumentException("Unable to get chunk for file restore process.");
                                        }
                                        
					//TODO Review protocol version
					GetMsg gMsg = new GetMsg("1.0", fileCh);
					gMsg.run();
					Thread.sleep(waitTime);
                                        
                                        runSendGetMsg = !file.exists();
                                        
                                        if(runSendGetMsg != false){
                                            tryNum++;
                                            waitTime*=2;
                                        }

				} while (runSendGetMsg);
			}

			//Rebuild file received chunks
			initFile.restoreFile();

		} catch (InterruptedException | IllegalArgumentException e) {
			System.err.println("Peer - restoreFile: " + e.getMessage());
		}

	}
}
