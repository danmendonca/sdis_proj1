package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;
import java.net.DatagramPacket;
import java.net.InetAddress;

public class ChunkMsg extends PeerMsg {

    private String chunkNoMsg;
    private byte bodyMsg[];
    int bytesRead;

    /**
     * Class Constructor
     *
     * @param rawData
     * @param bytesRead
     */
    public ChunkMsg(byte[] rawData, int bytesRead) {
        this.connectMsg = connectionType.RECEIVE;
        this.rawData = rawData;
        this.bytesRead = bytesRead;
        parseMsg(parseHeader());
    }

    /**
     * Class Constuctor
     *
     * @param versionM
     * @param chunkM
     */
    public ChunkMsg(String versionM, Chunk chunkM) {
        this.connectMsg = connectionType.SEND;
        this.typeMsg = messageType.CHUNK;
        this.versionMsg = versionM;
        this.fileIdMsg = chunkM.getFileId();
        this.chunkNoMsg = Integer.toString(chunkM.getChunkNo());
        this.bodyMsg = chunkM.getData();
    }

    /**
     *
     */
    @Override
    public void run() {
        if (this.connectMsg == connectionType.SEND) {
            runSender();
        } else if (this.connectMsg == connectionType.RECEIVE) {
            runReceiver();
        }
    }

    /**
     * Send chunk message
     */
    private void runSender() {
        //Send CHUNK message in MDR channel
        try {
        	Peer peer = Peer.getInstace();
            DatagramPacket chunkPacket = createPacket(peer.getMdrInet(), peer.getMdrPort());
            peer.sendMsgs(chunkPacket);
            System.out.println(this.typeMsg + " message sent.");
        } catch (Exception e) {
            System.out.println("ChunkMsg - runSender: " + e.getMessage());
        }
    }

    /**
     * Process received chunk message
     */
    private void runReceiver() {

        DBInteraction dbi = DBInteraction.getInstance();
        Chunk fileChunk = dbi.getFileChunk(this.fileIdMsg, Integer.parseInt(this.chunkNoMsg));

        //Check if we own the chunk
        if (fileChunk != null) {
            //Create restore chunk
            Chunk restChunk = new Chunk(this.fileIdMsg, this.chunkNoMsg, 0, this.bodyMsg);
            //Launch restoring process
            restChunk.restoreChunk();
        }
    }

    /**
     * Returns message Chunk Number
     *
     * @return Chunk Number
     */
    @Override
    public String getChunkNoMsg() {
        return chunkNoMsg;
    }

    /**
     * Returns message body with chunk data
     *
     * @return body with chunk data
     */
    @Override
    public byte[] getBodyMsg() {
        return bodyMsg;
    }

    /**
     * Parses and validates CHUNK messages
     *
     * @param msgHeader
     * @throws Exception
     */
    private void parseMsg(String[] msgHeader) {

        if (msgHeader.length != 4) {
            throw new IllegalArgumentException("Wrong number of message fields.");
        }

        parseVersion(msgHeader[1]);
        parseFileId(msgHeader[2]);
        parseChunkNo(msgHeader[3]);

        this.typeMsg = messageType.CHUNK;
        this.versionMsg = msgHeader[1];
        this.fileIdMsg = msgHeader[2];

        this.chunkNoMsg = msgHeader[3];
        parseBody();
    }

    /**
     * Parses message body and stores chunk data
     */
    private void parseBody() {     
        String strHeader = new String(this.rawData);
        String dataArray[] = strHeader.split("\\r\\n\\r\\n");
        int bodyFirstPos = dataArray[0].length() + 4;
        int bodySize = this.bytesRead - bodyFirstPos;

        this.bodyMsg = new byte[bodySize];
        System.arraycopy(this.rawData, bodyFirstPos, this.bodyMsg, 0, bodySize);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {

        StringBuilder strMsg = new StringBuilder();
        strMsg.append(super.toString());

        //Chunk No
        strMsg.append("Chunk No: ");
        if (this.chunkNoMsg != null) {
            strMsg.append(this.chunkNoMsg);
        }
        strMsg.append(System.lineSeparator());

        return strMsg.toString();
    }

    /**
     * Returns Datagram packet with message data
     *
     * @param packetIP
     * @param packetPort
     * @return
     */
    public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
        String cr = Character.toString((char) 13);
        String lf = Character.toString((char) 10);

        StringBuilder strBuild = new StringBuilder();

        //Type
        strBuild.append(this.typeMsg).append(" ");

        //Version
        strBuild.append(this.versionMsg).append(" ");

        //File ID
        strBuild.append(this.fileIdMsg);

        //Chunk No
        strBuild.append(" ");
        strBuild.append(this.chunkNoMsg);

        //CRLF CRLF
        strBuild.append(cr).append(lf).append(cr).append(lf);

        //Body
        //strBuild.append(this.bodyMsg);
        String packetStr = strBuild.toString();
        byte[] headerArray = packetStr.getBytes();
        int sizeHeader = headerArray.length;

        byte[] packetBuf = new byte[sizeHeader + this.bodyMsg.length];
        
        System.arraycopy(headerArray, 0, packetBuf, 0, sizeHeader);
	System.arraycopy(this.bodyMsg, 0, packetBuf, sizeHeader, this.bodyMsg.length);

        DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
        return packet;
    }

}
