package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DelMsg extends PeerMsg {

	/**
	 * Class Constructor
	 *
	 * @param rawData
	 */
	public DelMsg(byte[] rawData) {
		this.connectMsg = connectionType.RECEIVE;
		this.rawData = rawData;
		parseMsg(parseHeader());
	}

	/**
	 * Class Constructor
	 *
	 * @param versionM
	 * @param chunkM
	 */
	public DelMsg(String versionM, Chunk chunkM) {
		this.connectMsg = connectionType.SEND;
		this.typeMsg = messageType.DELETE;
		this.versionMsg = versionM;
		this.fileIdMsg = chunkM.getFileId();
	}

	/**
	 * 
	 * @param versionM
	 * @param fileId 
	 */
	public DelMsg(String versionM, String fileId){
		this.connectMsg = connectionType.SEND;
		this.typeMsg = messageType.DELETE;
		this.versionMsg = versionM;
		this.fileIdMsg = fileId;
	}

	/**
	 * Parses and validates DELETE messages
	 *
	 * @param msgHeader
	 * @throws Exception
	 */
	private void parseMsg(String[] msgHeader) {
		if (msgHeader.length != 3) {
			throw new IllegalArgumentException("Wrong number of message fields.");
		}

		parseVersion(msgHeader[1]);
		parseFileId(msgHeader[2]);

		this.typeMsg = messageType.DELETE;
		this.versionMsg = msgHeader[1];
		this.fileIdMsg = msgHeader[2];
	}

	/**
	 * Returns Datagram packet with message data
	 *
	 * @param packetIP
	 * @param packetPort
	 * @return
	 */
	public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
		String cr = Character.toString((char) 13);
		String lf = Character.toString((char) 10);

		StringBuilder strBuild = new StringBuilder();

		//Type
		strBuild.append(this.typeMsg).append(" ");

		//Version
		strBuild.append(this.versionMsg).append(" ");

		//File ID
		strBuild.append(this.fileIdMsg);

		//CRLF CRLF
		strBuild.append(cr).append(lf).append(cr).append(lf);

		String packetStr = strBuild.toString();
		byte[] packetBuf;
		packetBuf = packetStr.getBytes();

		DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
		return packet;
	}

	/* (non-Javadoc)
	 * @see message.PeerMsg#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(this.connectMsg == connectionType.RECEIVE)
			receiveDel();
		else
			sendDel();
	}

	/**
	 * 
	 */
	public void receiveDel(){
		//TODO test it
		DBInteraction dbi = DBInteraction.getInstance();
		dbi.deleteChunks(getFileIdMsg());
		dbi.deleteChunksPeers(getFileIdMsg());
	}

	/**
	 * 
	 */
	public void sendDel(){
		//
		try{
			Peer peer = Peer.getInstace();
			DatagramPacket packet = createPacket(peer.getMcInet(), peer.getMcPort());
			peer.sendMsgs(packet);
			System.out.println(this.typeMsg + " message sent.");
		}
		catch(UnknownHostException e){
			System.err.println("DelMsg - sendDel: " + e.getMessage());
		}
	}

}


