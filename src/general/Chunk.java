package general;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class Chunk {

	final public static String CHUNKS_PATH = "./chunks/";
	final public static String CHUNKS_RESTORE_PATH = "./restore/";

	String fileId;
	int chunkNo, replicationDegree, desiredRD;
	byte[] data;

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @param desiredRD
	 * @param data 
	 */
	public Chunk(String fileId, String chunkNo, int desiredRD, byte[] data){
		this.fileId = fileId;
		this.desiredRD = desiredRD;
		this.chunkNo = Integer.parseInt(chunkNo);
		this.data = data;
		this.replicationDegree = 0;
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @param desiredRD
	 * @param data 
	 */
	public Chunk(String fileId, int chunkNo, int desiredRD, byte[] data){
		this.fileId = fileId;
		this.chunkNo = chunkNo;
		this.desiredRD = desiredRD;
		this.data = data;
		this.replicationDegree = 0;
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 */
	public Chunk(String fileId, int chunkNo){
		//TODO
		this.fileId = fileId;
		this.chunkNo = chunkNo;
		this.replicationDegree = 0;
		this.desiredRD = 0;
	}

	/**
	 * set data from stored chunk in this file system
	 * if it is not found in filesystem, delete from database and try restore protocol
	 */
	protected boolean getDataFromFS() throws Exception{
		//TODO test it
		DBInteraction dbi = DBInteraction.getInstance();
		String path = dbi.getChunkPath(this.getFileId(), this.getChunkNo());
		File file = new File(path);

		if(file == null || !file.exists() || !file.isFile() || !file.canRead())
			throw new Exception("Unable to open file.");

		FileInputStream fis = new FileInputStream(file);
		this.data = new byte[(int)file.length()];
		fis.read(this.data);

		fis.close();
		//InitiatorPeerFile storedChunk = new InitiatorPeerFile(fileID);
		//this.data = storedChunk.getBytes();

		return true;
	}

	/**
	 * 
	 * @param path
	 * @return
	 * @throws Exception 
	 */
	protected boolean getDataFromFS(String path) throws Exception{
		//TODO test it
		File file = new File(path);

		if(file == null || !file.exists() || !file.isFile() || !file.canRead())
			throw new Exception("Unable to open file.");

		FileInputStream fis = new FileInputStream(file);
		this.data = new byte[(int)file.length()];
		fis.read(this.data);

		fis.close();
		//InitiatorPeerFile storedChunk = new InitiatorPeerFile(fileID);
		//this.data = storedChunk.getBytes();

		return true;
	}

	/**
	 * saves itself into CHUNKS table and its data into the FS
	 * @throws IOException
	 */
	public void saveChunk() throws IOException{

		DBInteraction dbi = DBInteraction.getInstance();
		String chunkPath = dbi.saveChunk(getFileId(), getChunkNo(), getDesiredRD(), getReplicationDegree());

		if(chunkPath != null){
			File file = new File(chunkPath);
			if(!file.exists())
				file.createNewFile();

			FileOutputStream fos = new FileOutputStream(file);
			fos.write(this.data);
			fos.close();
		}
	}

	/**
	 * saves itself into FILESCHUNKS 
	 */
	public void saveFileChunk(){
		DBInteraction dbi = DBInteraction.getInstance();		
		dbi.saveFileChunk(getFileId(), getChunkNo(), getDesiredRD(), getReplicationDegree());		
	}

	/**
	 * 
	 */
	public void restoreChunk(){
		String restorePath = CHUNKS_RESTORE_PATH + this.chunkNo + ".data";
		FileOutputStream fos;

		File file = new File(restorePath);

		if(file.exists())
			return;

		try {
			fos = new FileOutputStream(restorePath);
			fos.write(this.data);
			fos.close();
		} catch (IOException e) {
			System.err.println("Chunk - restoreChunk: " + e.getMessage());
		}

	}

	public int getChunkDegree(){
		return saveChunkDegree();
	}
	
 	public int saveChunkDegree(){
		DBInteraction dbi = DBInteraction.getInstance();
		this.replicationDegree= dbi.getChunkRepDegree(fileId, chunkNo) +1;
		dbi.saveChunkDegree(fileId, chunkNo, replicationDegree);
		return replicationDegree;
	}
 	 	
	public int getFileChunkDegree(){
		return saveFileChunkDegree();
	}
	
	public int saveFileChunkDegree(){
		DBInteraction dbi = DBInteraction.getInstance();
		this.replicationDegree= dbi.getFileChunkRepDegree(fileId, chunkNo);
		dbi.saveFileChunkDegree(fileId, chunkNo, replicationDegree);
		return replicationDegree;
	}

	/**
	 * 
	 * @param data 
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return the replicationDegree
	 */
	private int getReplicationDegree() {
		return replicationDegree;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String fileIdStr = "file id: " +fileId +'\n';
		String chunkNoStr = "chunkNo: " + String.valueOf(chunkNo) + '\n';
		String rdStr = "replicationDegree: "+ String.valueOf(replicationDegree)+'\n';
		return fileIdStr + chunkNoStr + rdStr;
	}

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the chunkNo
	 */
	public int getChunkNo() {
		return chunkNo;
	}

	/**
	 * @param chunkNo the chunkNo to set
	 */
	public void setChunkNo(int chunkNo) {
		this.chunkNo = chunkNo;
	}

	/**
	 * 
	 * @return 
	 */
	public String getChunkPath(){
		//TODO test it
		DBInteraction dbi = DBInteraction.getInstance();
		return dbi.getChunkPath(this.getFileId(), this.getChunkNo()); 

	}

	/**
	 * 
	 * @return 
	 */
	public int getDesiredRD(){
		return this.desiredRD;
	}

	/**
	 * 
	 * @param desiredRD 
	 */
	protected void setDesiredRD(int desiredRD){
		this.desiredRD = desiredRD;
	}

	/**
	 * 
	 */
	public void increaseDegree(){
		this.replicationDegree++;
	}

	/**
	 * Remove chunk from disk
	 * @param path
	 */
	public void removeFromDisk() {
		String path = getChunkPath(); 
		if (path != null) {
			File file = new File(path);
			file.delete();
		}
	}

	/**
	 * 
	 */
	public void removeChunkFromDb(){
		DBInteraction dbi = DBInteraction.getInstance();
		dbi.removeChunk(this.getFileId(), this.chunkNo);
		dbi.deleteChunksPeers(fileId);
	}

	public void deleteFileChunkPeers(){
		DBInteraction dbi = DBInteraction.getInstance();
		dbi.deleteFileChunkPeers(getFileId(), getChunkNo());
	}
}
