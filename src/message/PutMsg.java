package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

public class PutMsg extends PeerMsg {

	private String chunkNoMsg;
	private String repDegMsg;
	private byte bodyMsg[];
	private int bytesRead;
	
	/**
	 * Class Constructor
	 *
	 * @param rawData
	 */
	public PutMsg(byte[] rawData, int bytesRead) {
		this.connectMsg = connectionType.RECEIVE;
		this.rawData = rawData;
		this.bytesRead = bytesRead;
		parseMsg(parseHeader());
	}

	/**
	 * Class Constructor
	 *
	 * @param versionM
	 * @param chunkM
	 */
	public PutMsg(String versionM, Chunk chunkM) {
		this.connectMsg = connectionType.SEND;
		this.typeMsg = messageType.PUTCHUNK;
		this.versionMsg = versionM;
		this.fileIdMsg = chunkM.getFileId();
		this.chunkNoMsg = Integer.toString(chunkM.getChunkNo());
		this.repDegMsg = Integer.toString(chunkM.getDesiredRD());
		this.bodyMsg = chunkM.getData();
	}

	/**
	 * Parses and validates PUTCHUNK messages
	 *
	 * @param msgHeader
	 */
	private void parseMsg(String[] msgHeader) {

		if (msgHeader.length != 5) {
			throw new IllegalArgumentException("Wrong number of message fields.");
		}

		parseVersion(msgHeader[1]);
		parseFileId(msgHeader[2]);
		parseChunkNo(msgHeader[3]);
		parseRepDeg(msgHeader[4]);

		this.typeMsg = messageType.PUTCHUNK;
		this.versionMsg = msgHeader[1];
		this.fileIdMsg = msgHeader[2];
		this.chunkNoMsg = msgHeader[3];
		this.repDegMsg = msgHeader[4];
		this.parseBody();
	}

	/**
	 * Returns message Chunk Number
	 *
	 * @return Chunk Number
	 */
	@Override
	public String getChunkNoMsg() {
		return chunkNoMsg;
	}

	/**
	 * Returns message replication degree
	 *
	 * @return replication degree
	 */
	@Override
	public String getRepDegMsg() {
		return repDegMsg;
	}

	/**
	 * Returns message body with chunk data
	 *
	 * @return body with chunk data
	 */
	@Override
	public byte[] getBodyMsg() {
		return bodyMsg;
	}

	/**
	 * Parses and validates Replication Degree field
	 *
	 * @param mRepDeg
	 */
	private void parseRepDeg(String mRepDeg) {
		if (mRepDeg.length() != 1) {
			throw new IllegalArgumentException("Invalid replecation degree field");
		}
		Integer.parseInt(mRepDeg);
	}

	/**
	 * Parses message body and stores chunk data
	 */
	private void parseBody() {
		String strHeader = new String(this.rawData);
		String dataArray[] = strHeader.split("\\r\\n\\r\\n");
		int bodyFirstPos = dataArray[0].length() + 4;
		int bodySize = this.bytesRead - bodyFirstPos;

		this.bodyMsg = new byte[bodySize];
		System.arraycopy(this.rawData, bodyFirstPos, this.bodyMsg, 0, bodySize);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String toString() {

		StringBuilder strMsg = new StringBuilder();
		strMsg.append(super.toString());

		//Chunk No
		strMsg.append("Chunk No: ");
		if (this.chunkNoMsg != null) {
			strMsg.append(this.chunkNoMsg);
		}
		strMsg.append(System.lineSeparator());

		//Replication Degree
		strMsg.append("Rep. Degree: ");
		if (this.repDegMsg != null) {
			strMsg.append(this.repDegMsg);
		}
		strMsg.append(System.lineSeparator());

		return strMsg.toString();
	}

	/**
	 * Returns Datagram packet with message data
	 *
	 * @param packetIP
	 * @param packetPort
	 * @return
	 */
	public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
		String cr = Character.toString((char) 13);
		String lf = Character.toString((char) 10);

		StringBuilder strBuild = new StringBuilder();

		//Type
		strBuild.append(this.typeMsg).append(" ");

		//Version
		strBuild.append(this.versionMsg).append(" ");

		//File ID
		strBuild.append(this.fileIdMsg);

		//Chunk No
		strBuild.append(" ");
		strBuild.append(this.chunkNoMsg);

		//Replication Degree
		strBuild.append(" ");
		strBuild.append(this.repDegMsg);

		//CRLF CRLF
		strBuild.append(cr).append(lf).append(cr).append(lf);

		//Body
		//strBuild.append(this.bodyMsg);

		String packetStr = strBuild.toString();
                byte[] headerArray = packetStr.getBytes();
                int sizeHeader =  headerArray.length;
                
		byte[] packetBuf = new byte[sizeHeader + this.bodyMsg.length];
                
                System.arraycopy(headerArray, 0, packetBuf, 0, sizeHeader);
		System.arraycopy(this.bodyMsg, 0, packetBuf, sizeHeader, this.bodyMsg.length);
                
		DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
		return packet;
	}

	/* (non-Javadoc)
	 * @see message.PeerMsg#run()
	 */
	@Override
	public void run() {

		if(this.connectMsg == connectionType.RECEIVE)
			receivePut();
		else
			sendPut();
	}

        /**
         * 
         */
	public void receivePut(){

		DBInteraction dbi = DBInteraction.getInstance();	
		int chunkNo = Integer.parseInt(this.getChunkNoMsg());
		int desiredRP = Integer.parseInt(this.getRepDegMsg());

		//it's our message, just ignore it
		Chunk chunk = dbi.getFileChunk(this.fileIdMsg, chunkNo);
		if(chunk != null)
			return;

		
		
		try {
			Random timeGenerator = new Random();
			Thread.sleep(timeGenerator.nextInt(400));
			
			chunk = dbi.getChunk(this.fileIdMsg, chunkNo);
			
			//we don't have it
			if(chunk == null){
				chunk = new Chunk(this.fileIdMsg, chunkNo, desiredRP, this.bodyMsg);
				chunk.increaseDegree();
				chunk.saveChunk();
			}

			StoredMsg storedMsg = new StoredMsg(this.getVerMsg(), chunk);
			storedMsg.run();

		} catch (IOException | InterruptedException e) {
			System.err.println("PutMsg - receivePut: " + e.getMessage());
		}
	}
        
        /**
         * 
         */
	public void sendPut(){
		try{
			Peer peer = Peer.getInstace();
			DatagramPacket packet = this.createPacket(peer.getMdbInet(), peer.getMdbPort());
			peer.sendMsgs(packet);
                        System.out.println(this.typeMsg + " message sent.");
		}
		catch(UnknownHostException e){
			System.err.println(e.getMessage());
		}
	}

}
