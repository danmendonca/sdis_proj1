package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Random;

public class GetMsg extends PeerMsg {

    private String chunkNoMsg;

    /**
     *
     * @param rawData
     */
    public GetMsg(byte[] rawData) {
        this.connectMsg = connectionType.RECEIVE;
        this.rawData = rawData;
        parseMsg(parseHeader());
    }

    /**
     * Class Constructor
     *
     * @param versionM
     * @param chunkM
     */
    public GetMsg(String versionM, Chunk chunkM) {
        this.connectMsg = connectionType.SEND;
        this.typeMsg = messageType.GETCHUNK;
        this.versionMsg = versionM;
        this.fileIdMsg = chunkM.getFileId();
        this.chunkNoMsg = Integer.toString(chunkM.getChunkNo());
    }

    /**
     *
     */
    @Override
    public void run() {
        if (this.connectMsg == connectionType.SEND) {
            runSender();
        } else if (this.connectMsg == connectionType.RECEIVE) {
            runReceiver();
        }
    }

    /**
     * Send GETCHUNK message
     */
    private void runSender() {
        try {
        	Peer peer = Peer.getInstace();
            //Send GETCHUNK message in MC channel
            DatagramPacket getPacket = createPacket(peer.getMcInet(), peer.getMcPort());
            peer.sendMsgs(getPacket);
            System.out.println(this.typeMsg + " message sent.");
        } catch (Exception e) {
            System.out.println("GetMsg - runSender: " + e.getMessage());
        }
    }

    /**
     * Process received GETCHUNK message
     */
    private void runReceiver() {

        //Check if we are the initiator-peer
        DBInteraction dbi = DBInteraction.getInstance();
        Chunk ch = dbi.getFileChunk(this.fileIdMsg, Integer.parseInt(this.chunkNoMsg));

        if (ch == null) {

            //We aren't the initiator-peer
            //Check if we are storing the chunk in our DataBase
            ch = dbi.getChunk(this.fileIdMsg, Integer.parseInt(this.chunkNoMsg));

            try {
                if (ch != null) {

                	Peer peer = Peer.getInstace();
                    //Tell MDR channel to start storing the chunk messages
                    peer.getMDR().startListen();
                    //Waiting random time 0-400 ms
                    Random timeGenerator = new Random();
                    Thread.sleep(timeGenerator.nextInt(400));

                    //Tell MDR channel to stop storing the chunk messages
                    peer.getMDR().stopListen();

                    //Check if we received a message with the same chunk
                    if (peer.getMDR().findChunck(this.fileIdMsg, this.chunkNoMsg)) {
                        return;
                    }

                    //Create chunk message, that will be sent to the initiator-peer
                    ChunkMsg chunkM = new ChunkMsg(this.versionMsg, ch);
                    Thread t = new Thread(chunkM);
                    t.start();
                }
            } catch (InterruptedException e) {
                System.out.println("GetMsg - runReceiver: " + e.getMessage());
            }
        }

    }

    /**
     * Parses and validates GETCHUNK messages
     *
     * @param msgHeader
     */
    private void parseMsg(String[] msgHeader) {
        if (msgHeader.length != 4) {
            throw new IllegalArgumentException("Wrong number of message fields.");
        }

        parseVersion(msgHeader[1]);
        parseFileId(msgHeader[2]);
        parseChunkNo(msgHeader[3]);

        this.typeMsg = messageType.GETCHUNK;
        this.versionMsg = msgHeader[1];
        this.fileIdMsg = msgHeader[2];
        this.chunkNoMsg = msgHeader[3];
    }

    /**
     * Returns message Chunk Number
     *
     * @return Chunk Number
     */
    @Override
    public String getChunkNoMsg() {
        return chunkNoMsg;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {

        StringBuilder strMsg = new StringBuilder();
        strMsg.append(super.toString());

        //Chunk No
        strMsg.append("Chunk No: ");
        if (this.chunkNoMsg != null) {
            strMsg.append(this.chunkNoMsg);
        }
        strMsg.append(System.lineSeparator());

        return strMsg.toString();
    }

    /**
     * Returns Datagram packet with message data
     *
     * @param packetIP
     * @param packetPort
     * @return
     */
    public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
        String cr = Character.toString((char) 13);
        String lf = Character.toString((char) 10);

        StringBuilder strBuild = new StringBuilder();

        //Type
        strBuild.append(this.typeMsg).append(" ");

        //Version
        strBuild.append(this.versionMsg).append(" ");

        //File ID
        strBuild.append(this.fileIdMsg);

        //Chunk No
        strBuild.append(" ");
        strBuild.append(this.chunkNoMsg);

        //CRLF CRLF
        strBuild.append(cr).append(lf).append(cr).append(lf);

        String packetStr = strBuild.toString();
        byte[] packetBuf;
        packetBuf = packetStr.getBytes();

        DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
        return packet;
    }
}
