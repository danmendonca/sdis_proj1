package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Random;

public class RemMsg extends PeerMsg {

    private String chunkNoMsg;
	private String fromAddress;
	private int fromPort;
	
	public String getFromAddress() {
		return fromAddress;
	}

	public int getFromPort() {
		return fromPort;
	}

    /**
     * Class Constructor
     *
     * @param rawData
     */
    public RemMsg(DatagramPacket packet) {
        this.connectMsg = connectionType.RECEIVE;
        this.rawData = packet.getData();
        
		fromPort = packet.getPort();
		fromAddress = packet.getAddress().getHostAddress();
        
        parseMsg(parseHeader());
    }

    /**
     * Class Constructor
     *
     * @param versionM
     * @param chunkM
     */
    public RemMsg(String versionM, Chunk chunkM) {
        this.connectMsg = connectionType.SEND;
        this.typeMsg = messageType.REMOVED;
        this.versionMsg = versionM;
        this.fileIdMsg = chunkM.getFileId();
        this.chunkNoMsg = Integer.toString(chunkM.getChunkNo());
    }

    /**
     *
     */
    @Override
    public void run() {
        if (this.connectMsg == connectionType.SEND) {
            runSender();
        } else if (this.connectMsg == connectionType.RECEIVE) {
            runReceiver();
        }
    }

    /**
     * Send REMOVED message
     */
    private void runSender() {

        try {
        	Peer peer = Peer.getInstace();      
            //Send REMOVED message in MC channel
            DatagramPacket remPacket = createPacket(peer.getMcInet(), peer.getMcPort());
            peer.sendMsgs(remPacket);
            System.out.println(this.typeMsg + " message sent.");
        } catch (Exception e) {
            System.out.println("RemMsg - runSender: " + e.getMessage());
        }
    }

    /**
     * Process received REMOVED message
     */
    private void runReceiver() {
        
        //CHUNK OWNER ======================
        //Verify if we own the removed chunk
        DBInteraction dbi = DBInteraction.getInstance();
        Chunk savedChunk = dbi.getFileChunk(this.fileIdMsg, Integer.parseInt(this.chunkNoMsg));

        if (savedChunk != null) {
            //Decrease actual replication degree
            dbi.deleteFileChunkPeer(fromAddress, fromPort, savedChunk.getFileId(), savedChunk.getChunkNo());
            return;
        }

        //CHUNK STORED ======================
        //Check if we are storing the chunk in our DataBase
        savedChunk = dbi.getChunk(this.fileIdMsg, Integer.parseInt(this.chunkNoMsg));

        if (savedChunk != null) {
            try {
                //Update local count of the chunk
            	dbi.deleteChunkPeer(fromAddress, fromPort, savedChunk.getFileId(), savedChunk.getChunkNo());
                
                //Check if actual replication degree is lower than desired replication degree
                if (savedChunk.getChunkDegree() < savedChunk.getDesiredRD()) {
                    
                    Peer peer = Peer.getInstace();
                    //Tell MDB channel to start storing received PUTCHUNK messages
                    peer.getMDB().startListen();                    
                    
                    //Waiting random time 0-400 ms
                    Random timeGenerator = new Random();
                    Thread.sleep(timeGenerator.nextInt(400));
                    
                    //Tell MDB channel to stop storing received PUTCHUNK messages 
                    peer.getMDB().stopListen();
                    
                    //Check if we received a PUTCHUNK message with the same chunk
                    if(peer.getMDB().findChunck(fileIdMsg, chunkNoMsg)){
                        
                        peer.getMDB().startListen();
                        Thread.sleep(Peer.MAX_BACKUP_WAIT_TIME+timeGenerator.nextInt(400));
                        peer.getMDB().stopListen();
                        
                        if (savedChunk.getDesiredRD() <= savedChunk.getChunkDegree()) {
                            return;
                        }
                        
                        ArrayList<Chunk> chList = peer.getMDB().getChunkList();
                        int count = 0;
                        for(Chunk ch : chList){
                            if((ch.getChunkNo() == savedChunk.getChunkNo()) && ch.getFileId().equals(savedChunk.getFileId())){
                                count++;
                            }
                        }
                        
                        if(count >= 5){
                            return;
                        }
                    }
                    
                    //Create PUTCHUNK message and send it
                    int time = Peer.MIN_BACKUP_WAIT_TIME;
                    do {
                        //TODO version
                        PutMsg putMsg = new PutMsg(versionMsg, savedChunk);
                        putMsg.run();

                        Thread.sleep(time);

                        if (savedChunk.getDesiredRD() <= savedChunk.getChunkDegree()) {
                            break;
                        }

                        //leave as it is after last try
                       // if (time != Peer.MAX_BACKUP_WAIT_TIME) {
                           // savedChunk.resetRD();
                       // }

                        time *= 2;
                    } while (time <= Peer.MAX_BACKUP_WAIT_TIME);
                    
                }
                
            } catch (InterruptedException e) {
                System.out.println("RemMsg - runReceiver: " + e.getMessage());
            }
        }
    }

    /**
     * Parses and validates REMOVED messages
     *
     * @param msgHeader
     */
    private void parseMsg(String[] msgHeader) {

        if (msgHeader.length != 4) {
            throw new IllegalArgumentException("Wrong number of message fields.");
        }

        parseVersion(msgHeader[1]);
        parseFileId(msgHeader[2]);
        parseChunkNo(msgHeader[3]);

        this.typeMsg = messageType.REMOVED;
        this.versionMsg = msgHeader[1];
        this.fileIdMsg = msgHeader[2];
        this.chunkNoMsg = msgHeader[3];
    }

    /**
     * Returns message Chunk Number
     *
     * @return Chunk Number
     */
    @Override
    public String getChunkNoMsg() {
        return chunkNoMsg;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {

        StringBuilder strMsg = new StringBuilder();
        strMsg.append(super.toString());

        //Chunk No
        strMsg.append("Chunk No: ");
        if (this.chunkNoMsg != null) {
            strMsg.append(this.chunkNoMsg);
        }
        strMsg.append(System.lineSeparator());

        return strMsg.toString();
    }

    /**
     * Returns Datagram packet with message data
     *
     * @param packetIP
     * @param packetPort
     * @return
     */
    public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
        String cr = Character.toString((char) 13);
        String lf = Character.toString((char) 10);

        StringBuilder strBuild = new StringBuilder();

        //Type
        strBuild.append(this.typeMsg).append(" ");

        //Version
        strBuild.append(this.versionMsg).append(" ");

        //File ID
        strBuild.append(this.fileIdMsg);

        //Chunk No
        strBuild.append(" ");
        strBuild.append(this.chunkNoMsg);

        //CRLF CRLF
        strBuild.append(cr).append(lf).append(cr).append(lf);

        String packetStr = strBuild.toString();
        byte[] packetBuf;
        packetBuf = packetStr.getBytes();

        DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
        return packet;
    }
}
