package general;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import message.PeerMsg;

public class MDBWrapper implements Runnable {

    MulticastSocket mdb;
    final private int DATASIZE = 80000;

    private Boolean runChannel = true;
    private Boolean listenChannel = false;
    private ArrayList<Chunk> chunkList;

    /**
     * Class Constructor
     *
     * @param address
     * @param port
     * @throws IOException
     */
    public MDBWrapper(String address, int port) throws IOException {
        this.chunkList = new ArrayList<>();
        InetAddress addr = InetAddress.getByName(address);
        mdb = new MulticastSocket(port);
        mdb.joinGroup(addr);
    }
    
    /**
     * Ends while cycle in run() and closes socket
     */
    public void closeChannel() {
        this.runChannel = false;
        mdb.close();
    }

    /**
     * Thread Method
     */
    @Override
    public void run() {

        while (runChannel) {
            byte[] data = new byte[DATASIZE];
            DatagramPacket packet = new DatagramPacket(data, DATASIZE);

            try {
                //Receive and process datagram packet
                mdb.receive(packet);
                int bytesRead = packet.getLength();
                PeerMsg pMsg = PeerMsg.getInstance(packet, bytesRead);
                System.out.println("MDB: " + pMsg.getTypeMsg() + " message received.");

                //Verify valid messge type
                if (pMsg.getTypeMsg() == PeerMsg.messageType.PUTCHUNK) {
                	
                	//TODO explain
                    //If we are listenning to the MDB channel, than we will log the chunk arrival
                    if (listenChannel) {
                        chunkList.add(new Chunk(pMsg.getFileIdMsg(), Integer.parseInt(pMsg.getChunkNoMsg())));
                    }
                    
                    //Launch PUTCHUNK message processing
                    Thread t = new Thread(pMsg);
                    t.start();
                }
                
                
                
            } catch (IOException e) {
                System.err.println("MDBWrapper - run: " + e.getMessage());
            }
        }
    }

    /**
     * Starts listening to the channel and saving the received PUTCHUNK messages
     * in the chunkList
     */
    public void startListen() {
        this.chunkList = new ArrayList<>();
        this.listenChannel = true;
    }

    /**
     * Stops listening to the channel and saving the received PUTCHUNK messages
     * in the chunkList
     */
    public void stopListen() {
        this.listenChannel = false;
    }

    /**
     * Checks if we received an specified chunk
     * @param fileId
     * @param chunkNo
     * @return 
     */
    public Boolean findChunck(String fileId, String chunkNo) {
        for (Chunk ch : this.chunkList) {
            if (ch.getFileId().equals(fileId) && Integer.toString(ch.getChunkNo()).equals(chunkNo)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @return 
     */
    public ArrayList<Chunk> getChunkList(){
        return this.chunkList;
    }
    
}
