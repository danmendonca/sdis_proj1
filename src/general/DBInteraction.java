package general;
import java.sql.*;
import java.util.ArrayList;
import java.io.File;

public class DBInteraction {

	public static final String DEFAULT_DB = "database";
	protected static final String FILESCHUNKS_TABLE = "FILESCHUNKS";
	protected static final String CHUNKS_TABLE = "CHUNKS";
	private static Statement statement;
	private static Connection connection;
	private static DBInteraction dbi;
	private static boolean DEBUG = true;
	private static boolean available = true;
	private static int insideSynchronized = 0;



	public static boolean isDEBUG() {
		return DEBUG;
	}

	/**
	 * 
	 * @return 
	 */	
	public static DBInteraction getInstance(){
		if(dbi==null)
			dbi = new DBInteraction(null);
		return dbi;
	}

	/**
	 * 
	 * @param dbPath 
	 */
	private DBInteraction(String dbPath){
		if(dbPath == null)
			dbPath = DEFAULT_DB;
		try{
			connection = DriverManager.getConnection("jdbc:sqlite:"+dbPath);

			statement = connection.createStatement();
			String query = "SELECT name FROM sqlite_master WHERE type='table';";

			ResultSet resSet = statement.executeQuery(query);

			if(resSet.isAfterLast()){
				createTables();
			}
			closeStatement();
		}catch(SQLException e){
			System.err.println("DBInteraction - Constructor: " + e.getErrorCode());
			System.err.println("DBInteraction - Constructor: " + e.getMessage());
			System.err.println("DBInteraction - Constructor: " + e.getSQLState());
		}
		closeStatement();		
	}

	/**
	 * 
	 * @return 
	 */
	protected boolean createTables(){
		try{

			String createTableFiles = "CREATE TABLE FILES" +
					" (FILENAME CHAR NOT NULL," +
					" FILEID VARCHAR(64) NOT NULL," +
					" NRCHUNKS CHAR(10)," +
					" DESIRED_RD CHAR(1)," +
					" PRIMARY KEY(FILENAME));";

			String createTableFilesChunks	 = "CREATE TABLE FILESCHUNKS" +
					" (FILEID CHAR(100) NOT NULL," +
					" CHUNKNO CHAR(50)," +
					" DESIRED_RD CHAR(1)," +
					" REPLICATION_DEGREE CHAR(2),"+
					" FOREIGN KEY(FILEID) REFERENCES FILES(FILEID));";

			String createTableChunks = "CREATE TABLE CHUNKS" +
					" (FILENAME INTEGER UNIQUE NOT NULL," +
					" FILEID CHAR NOT NULL," +
					" CHUNKNO CHAR(10) NOT NULL," +
					" DESIRED_RD CHAR(1) NOT NULL," +
					" REPLICATION_DEGREE CHAR(2), PRIMARY KEY(FILEID, CHUNKNO));";

			String createTableChunksPeers = "CREATE TABLE CHUNKSPEERS (" +
					" IP CHAR," +
					" PORT CHAR," +
					" FILEID CHAR," +
					" CHUNKNO CHAR(50),"+
					" FOREIGN KEY(FILEID) REFERENCES CHUNKS(FILEID)," +
					" FOREIGN KEY(CHUNKNO) REFERENCES CHUNKS(CHUNKNO)," +
					" PRIMARY KEY(IP, PORT, FILEID, CHUNKNO)" +
					");";
			String createTableFilesChunksPeers = "CREATE TABLE FILESCHUNKSPEERS (" +
					" IP CHAR," +
					" PORT CHAR," +
					" FILEID CHAR," +
					" CHUNKNO CHAR(50),"+
					" FOREIGN KEY(FILEID) REFERENCES FILESCHUNKS(FILEID)," +
					" FOREIGN KEY(CHUNKNO) REFERENCES FILESCHUNKS(CHUNKNO)," +
					" PRIMARY KEY(IP, PORT, FILEID, CHUNKNO)" +
					");";

			statement = connection.createStatement();
			statement.executeUpdate(createTableFiles);
			closeStatement();

			statement = connection.createStatement();
			statement.executeUpdate(createTableFilesChunks);
			closeStatement();

			statement = connection.createStatement();
			statement.executeUpdate(createTableChunks);

			statement.executeUpdate(createTableChunksPeers);
			statement.executeUpdate(createTableFilesChunksPeers);

		}catch(SQLException e){
			System.err.println("DBInteraction - createTables: " + e.getErrorCode());
			System.err.println("DBInteraction - createTables: " + e.getMessage());
			System.err.println("DBInteraction - createTables: " + e.getSQLState());
		}

		return closeStatement();
	}

	/**
	 * 
	 */
	public void closeConnection(){
		try {
			connection.close();
		} catch (SQLException e) {
			System.err.println("DBInteraction - closeConnection: " + e.getMessage());
		}
	}

	/**
	 * 
	 * @return 
	 */
	public boolean closeStatement(){
		try {
			statement.close();
			setAvailableTrue();
			return true;
		} catch (SQLException e) {
			System.err.println("DBInteraction - closeStatement: " + e.getSQLState());
		}
		return false;
	}


	protected void openStatement(){

		try {
			setAvailableFalse();
			statement = connection.createStatement();
		} catch (SQLException e) {
			System.err.println(e.getSQLState());
			System.err.println(e.getMessage());
			System.err.println(" on createStatement()");
		}
	}


	/*
	 * 
	 * 
	 * CHUNKSPEERS & FILECHUNKSPEERS
	 * 
	 * 
	 */

	/**
	 * 
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 */
	public void insertChunkPeer(String ip, int port, String fileId, int chunkNo){

		String insert = "INSERT INTO CHUNKSPEERS (IP, PORT, FILEID, CHUNKNO)"
				+ "VALUES ('"
				+ ip + "', '"
				+ String.valueOf(port) + "', '"
				+ fileId + "', '" 
				+ String.valueOf(chunkNo)+"');";
		openStatement();
		try {
			statement.executeUpdate(insert);
		} catch (SQLException e) {
			System.err.println("DBInteraction - insertChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - insertChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - insertChunkPeer: " + e.getSQLState());
		}
		closeStatement();
	}

	/**
	 * 
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 */
	public void insertFileChunkPeer(String ip, int port, String fileId, int chunkNo){

		String insert = "INSERT INTO FILESCHUNKSPEERS (IP, PORT, FILEID, CHUNKNO)"
				+ "VALUES ('"
				+ ip + "', '"
				+ String.valueOf(port) + "', '"
				+ fileId + "', '" 
				+ String.valueOf(chunkNo)+"');";
		openStatement();
		try {
			statement.executeUpdate(insert);
		} catch (SQLException e) {
			System.err.println("DBInteraction - insertFileChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - insertFileChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - insertFileChunkPeer: " + e.getSQLState());
		}
		closeStatement();
	}


	/**
	 * 
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 * @return true if pair (ip, port) already are associated with that chunk
	 */
	public boolean isChunkPeer(String ip, int port, String fileId, int chunkNo){

		boolean isChunkPeer = false;

		String query = "SELECT * FROM CHUNKSPEERS WHERE" +
				" IP ='" + ip + "' AND" +
				" PORT ='" + String.valueOf(port) + "' AND" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			ResultSet ans = statement.executeQuery(query);

			if(!ans.isAfterLast())
				isChunkPeer = true;

		} catch (SQLException e) {
			System.err.println("DBInteraction - isChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - isChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - isChunkPeer: " + e.getSQLState());
		}

		closeStatement();
		return isChunkPeer;
	}

	/**
	 * 
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 * @return true if pair (ip, port) already are associated with that fileChunk
	 */
	public boolean isFileChunkPeer(String ip, int port, String fileId, int chunkNo){

		boolean isFileChunkPeer = false;

		String query = "SELECT * FROM FILESCHUNKSPEERS WHERE" +
				" IP ='" + ip + "' AND" +
				" PORT ='" + String.valueOf(port) + "' AND" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			ResultSet ans = statement.executeQuery(query);

			if(!ans.isAfterLast())
				isFileChunkPeer = true;

		} catch (SQLException e) {
			System.err.println("DBInteraction - isChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - isChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - isChunkPeer: " + e.getSQLState());
		}

		closeStatement();
		return isFileChunkPeer;
	}

	/**
	 * Deletes all chunkPeers associated to a chunk with given fileId and chunkNo
	 * @param fileId
	 * @param chunkNo
	 */
	public void deleteChunkPeers(String fileId, int chunkNo){

		String deleteCPs = "DELETE FROM CHUNKSPEERS WHERE" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			statement.executeQuery(deleteCPs);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteChunkPeers: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteChunkPeers: " + e.getMessage());
			System.err.println("DBInteraction - deleteChunkPeers: " + e.getSQLState());
		}

		closeStatement();
	}


	/**
	 * Deletes all fileChunkPeers associated to a fileChunk with given fileId and chunkNo
	 * @param fileId
	 * @param chunkNo
	 */
	public void deleteFileChunkPeers(String fileId, int chunkNo){



		String deleteFCPs = "DELETE FROM FILESCHUNKSPEERS WHERE" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			statement.executeQuery(deleteFCPs);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteFileChunkPeers: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteFileChunkPeers: " + e.getMessage());
			System.err.println("DBInteraction - deleteFileChunkPeers: " + e.getSQLState());
		}

		closeStatement();
	}


	/**
	 * delete a chunkPeer with the pair (ip, port) associated with the chunk
	 * with given fileId and chunkNo
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 */
	public void deleteChunkPeer(String ip, int port, String fileId, int chunkNo){



		String query = "DELETE FROM CHUNKSPEERS WHERE" +
				" IP ='" + ip + "' AND" +
				" PORT ='" + String.valueOf(port) + "' AND" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			statement.executeQuery(query);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - deleteChunkPeer: " + e.getSQLState());
		}

		closeStatement();
	}

	/**
	 * delete a fileChunkPeer with the pair (ip, port) associated with the fileChunk
	 * with given fileId and chunkNo
	 * @param ip
	 * @param port
	 * @param fileId
	 * @param chunkNo
	 */
	public void deleteFileChunkPeer(String ip, int port, String fileId, int chunkNo){



		String query = "DELETE FROM FILESCHUNKSPEERS WHERE" +
				" IP ='" + ip + "' AND" +
				" PORT ='" + String.valueOf(port) + "' AND" +
				" FILEID ='" + fileId + "' AND"+
				" CHUNKNO ='" + String.valueOf(chunkNo) + "';";

		openStatement();

		try {
			statement.executeQuery(query);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getSQLState());
		}

		closeStatement();
	}
	
	
	/**
	 * deletes all chunkPeers of the chunks with the given fileId
	 * @param fileId
	 */
	public void deleteChunksPeers(String fileId){

		String query = "DELETE FROM CHUNKSPEERS WHERE" +
				" FILEID ='" + fileId + "';";

		openStatement();

		try {
			statement.executeQuery(query);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getSQLState());
		}

		closeStatement();
	}
	
	
	/**
	 * deletes all fileChunksPeers of the fileChunks with the given fileId
	 * @param fileId
	 */
	public void deleteFileChunksPeers(String fileId){



		String query = "DELETE FROM FILESCHUNKSPEERS WHERE" +
				" FILEID ='" + fileId + "';";

		openStatement();

		try {
			statement.executeQuery(query);			
		} catch (SQLException e) {
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getMessage());
			System.err.println("DBInteraction - deleteFileChunkPeer: " + e.getSQLState());
		}

		closeStatement();
	}

	/*
	 * 
	 * 
	 * FILES 
	 * 
	 * 
	 * 
	 */

	/**
	 * 
	 * @param initiatorPeerFile 
	 */
	public void saveFile(InitiatorPeerFile initiatorPeerFile){

		saveFile(initiatorPeerFile.getFileName(), initiatorPeerFile.getFileId(), 
				initiatorPeerFile.getNrChunks(), initiatorPeerFile.getdesiredRD());
	}

	/**
	 * 
	 * @param fileName
	 * @param fileId
	 * @param nrChunks
	 * @param replicationDegree 
	 */
	public void saveFile(String fileName, String fileId, 
			int nrChunks, int replicationDegree){

		String insert = "INSERT INTO FILES (FILENAME, FILEID, NRCHUNKS, "
				+ "DESIRED_RD) VALUES ('"
				+ fileName + "', '"
				+ fileId + "', '"
				+ String.valueOf(nrChunks) + "', '" 
				+ String.valueOf(replicationDegree)+"');";
		openStatement();
		
		try{		
			statement.executeUpdate(insert);

		}
		catch(SQLException e){
			System.err.println("DBInteraction - saveFile: " + e.getErrorCode());
			System.err.println("DBInteraction - saveFile: " + e.getMessage());
			System.err.println("DBInteraction - saveFile: " + e.getSQLState());
		}
		closeStatement();
	}

	/**
	 * 
	 * @param fileName 
	 */
	public void deleteFile(String fileName){
		//TODO test it
		try{

			String deleteFCs = "DELETE from FILES where FILENAME='"
					+ fileName + "';";
			openStatement();
			statement.executeUpdate(deleteFCs);
		}
		catch(SQLException e){
			System.err.println("DBInteraction - deleteFile: " + e.getMessage());
		}
		closeStatement();
	}

	/**
	 * 
	 * @param fileName
	 * @return 
	 */
	public InitiatorPeerFile getFile(String fileName){

		InitiatorPeerFile file = null;
		try{


			String query = "SELECT * FROM FILES WHERE FILENAME= '" + fileName +"';";

			openStatement();
			ResultSet ans = statement.executeQuery(query);


			if(!ans.isAfterLast()){
				String fileId = ans.getString("FILEID");
				int nrChunks = Integer.parseInt((ans.getString("NRCHUNKS")));
				int replicationDegree = Integer.parseInt((ans.getString("DESIRED_RD")));
				file = new InitiatorPeerFile(fileName, replicationDegree, fileId, nrChunks);
			}
		}
		catch(SQLException e){
			System.err.println("DBInteraction - getFile: " + e.getErrorCode());
			System.err.println("DBInteraction - getFile: " + e.getMessage());
			System.err.println("DBInteraction - getFile: " + e.getSQLState());
		}
		catch(Exception e){
			System.err.println("DBInteraction - getFile: " + e.getMessage());
		}

		closeStatement();
		return file;
	}

	/*
	 * 
	 * 
	 * FILESCHUNKS
	 * 
	 * 
	 * 
	 */

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @param desiredRD
	 * @param replicationDegree
	 * @return 
	 */
	public boolean saveFileChunk(String fileId, int chunkNo,
			int desiredRD, int replicationDegree){
		String insert = "INSERT INTO FILESCHUNKS (FILEID, CHUNKNO, "
				+ "DESIRED_RD, REPLICATION_DEGREE) VALUES ('" 
				+ fileId + "', '"
				+ String.valueOf(chunkNo) + "', '"
				+ String.valueOf(desiredRD) + "', '"
				+ String.valueOf(replicationDegree)+"');";

		openStatement();
		try{		
			statement.executeUpdate(insert);

		}
		catch(Exception e){
			System.err.println("DBInteraction - saveFileChunk: " + e.getMessage());
		}

		return closeStatement();
	}
//
//	/**
//	 * 
//	 * @param chunk 
//	 */
//	public void saveFileChunkRDegree(Chunk chunk){
//
//		saveFileChunkRDegree(chunk.fileId, chunk.chunkNo, chunk.replicationDegree);
//	}
//
//	/**
//	 * 
//	 * @param fileId
//	 * @param chunkNo
//	 * @param replicationDegree 
//	 */
//	public void saveFileChunkRDegree(String fileId, int chunkNo, int replicationDegree){
//		//TODO test it
//		//int reply = 0;
//		try{		
//
//
//			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE='" 
//					+ String.valueOf(replicationDegree)
//					+ "' WHERE FILEID='" + fileId
//					+ "' AND CHUNKNO='" + String.valueOf(chunkNo)
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - saveFileChunkRDegree: " + e.getMessage());
//		}
//
//		closeStatement();
//	}
//
//	/**
//	 * 
//	 * @param fileId
//	 * @param chunkNo
//	 * @return 
//	 */
	public Chunk getFileChunk(String fileId, int chunkNo){

		Chunk chunk = null;
		String query = "SELECT * FROM FILESCHUNKS" 
				+" WHERE FILEID= '" + fileId 
				+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";

		openStatement();
		try{
			ResultSet ans = statement.executeQuery(query);

			if(!ans.isAfterLast()){
				chunk = new Chunk(fileId, chunkNo);
				int desiredDegree = Integer.parseInt((ans.getString("DESIRED_RD")));
				chunk.desiredRD = desiredDegree;
			}

		}
		catch(Exception e){
			System.err.println("DBInteraction - getFileChunk: " + e.getMessage());
		}
		closeStatement();
		if(chunk!=null)
			chunk.replicationDegree = getFileChunkRepDegree(fileId, chunkNo);
		
		return chunk;
	}
//
//	/**
//	 * 
//	 * @param chunk
//	 * @return 
//	 */
	public int getFileChunkRepDegree(Chunk chunk){

		return getFileChunkRepDegree(chunk.fileId, chunk.chunkNo);
	}
//
//	/**
//	 * 
//	 * @param fileId
//	 * @param chunkNo
//	 * @return 
//	 */
//	public int getFileChunkRepDegree(String fileId, int chunkNo){
//		//TODO test it
//		int fileChunkRD = -1;
//
//		try{		
//
//
//			String query = "SELECT REPLICATION_DEGREE FROM '"
//					+ FILESCHUNKS_TABLE 
//					+"' WHERE FILEID= '" + fileId 
//					+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";
//			openStatement();
//			ResultSet ans = statement.executeQuery(query);
//
//			if(!ans.isAfterLast()){
//				String replicationDegree = ans.getString("REPLICATION_DEGREE");
//				fileChunkRD = Integer.parseInt(replicationDegree);
//			}
//
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - getFileChunkRepDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - getFileChunkRepDegree: " + e.getMessage());
//			System.err.println("DBInteraction - getFileChunkRepDegree: " + e.getSQLState());
//
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - getFileChunkRepDegree: " + e.getMessage());
//		}
//		closeStatement();
//		return fileChunkRD;
//	}
//
//	/**
//	 * 
//	 * @param chunk 
//	 */
//	public void increaseFileChunkDegree(Chunk chunk){
//		//TODO test it
//		try{		
//
//
//			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE='" 
//					+ String.valueOf(chunk.getReplicationDegree()+1)
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - increaseFileChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - increaseFileChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - increaseFileChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - increaseFileChunkDegree: " + e.getMessage());
//		}
//
//		closeStatement();
//	}
//
//	/**
//	 * 
//	 * @param chunk 
//	 */
//	public void decreaseFileChunkDegree(Chunk chunk){
//		//TODO test it
//		try{		
//			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE='" 
//					+ String.valueOf(chunk.getReplicationDegree()-1)
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - decreaseFileChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - decreaseFileChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - decreaseFileChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - decreaseFileChunkDegree: " + e.getMessage());
//		}
//
//		closeStatement();
//	}

	
	public boolean saveFileChunkDegree(String fileId, int chunkNo, int replicationDegree){
		//TODO test it
		try{		


			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE='" 
					+ String.valueOf(replicationDegree)
					+ "' WHERE FILEID='" + fileId
					+ "' AND CHUNKNO='" + String.valueOf(chunkNo)
					+ "';";
			openStatement();
			statement.executeUpdate(update);
			closeStatement();
			return true;
		}
		catch(Exception e){
			System.err.println("DBInteraction - saveChunkDegree: " + e.getMessage());
			closeStatement();
		}
		return false;
	}
	
	public int getFileChunkRepDegree(String fileId, int chunkNo){
		
		int rd = 0;
		String query = "SELECT COUNT(*) AS TOTAL FROM FILESCHUNKSPEERS WHERE"+
				" FILEID = '"+ fileId + "' AND CHUNKNO='"+ chunkNo +"';";
		
		openStatement();
		try {
			ResultSet ans = statement.executeQuery(query);
			while(ans.next())
				rd=ans.getInt("TOTAL");
		} catch (SQLException e) {
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getErrorCode());
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getMessage());
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getSQLState());
		}
		closeStatement();
		return rd;
	}
	
	/**
	 * 
	 * @param chunk a chunk to remove from database
	 */
	public void removeFileChunk(Chunk chunk){
		removeFileChunk(chunk.getFileId(), chunk.getChunkNo());
	}

	/**
	 * remove a filechunk from the database
	 * @param fileId
	 * @param chunkNo
	 */
	public void removeFileChunk(String fileId, int chunkNo){
		//TODO test it
		try{		

			String deleteFC = "DELETE FROM FILESCHUNKS WHERE "
					+ "ID='"+ fileId
					+ "' AND CHUNKNO="+ chunkNo + ";";
			openStatement();
			statement.executeUpdate(deleteFC);

		} catch(SQLException e){
			System.err.println("DBInteraction - removeFileChunk: " + e.getErrorCode());
			System.err.println("DBInteraction - removeFileChunk: " + e.getMessage());
			System.err.println("DBInteraction - removeFileChunk: " + e.getSQLState());
		}
		catch(Exception e){
			System.err.println("DBInteraction - removeFileChunk: " + e.getMessage());
		}
		closeStatement();
	}

	/**
	 * 
	 * @param chunk 
	 */
//	public void resetFileChunkDegree(Chunk chunk){
//		//TODO test it
//		try{		
//
//
//			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE= 0'" 
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getMessage());
//		}
//
//		closeStatement();
//	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo 
	 */
//	public void resetFileChunkDegree(String fileId, int chunkNo){
//		//TODO test it
//		try{		
//
//
//			String update = "UPDATE FILESCHUNKS SET REPLICATION_DEGREE= 0" 
//					+ " WHERE FILEID='" + fileId
//					+ "' AND CHUNKNO='" + String.valueOf(chunkNo)
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - resetFileChunkDegree: " + e.getMessage());
//		}
//
//		closeStatement();
//	}

	/**
	 * delete all filechunk with the given fileId
	 * @param fileId
	 */
	public void deleteFileChunks(String fileId){
		//TODO test it
		try{		

			String deleteFCs = "DELETE from FILESCHUNKS where FILEID='"
					+ fileId + "';";
			openStatement();
			statement.executeUpdate(deleteFCs);

		} catch(SQLException e){
			System.err.println("DBInteraction - deleteFileChunks: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteFileChunks: " + e.getMessage());
			System.err.println("DBInteraction - deleteFileChunks: " + e.getSQLState());
		}
		catch(Exception e){
			System.err.println("DBInteraction - deleteFileChunks: " + e.getMessage());
		}

		closeStatement();
	}

	/*
	 * 
	 * 
	 * CHUNKS
	 * 
	 * 
	 * 
	 */

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @param desiredRD
	 * @param rd
	 * @return 
	 */
	public String saveChunk(String fileId, int chunkNo, int desiredRD, int rd){

		try{		


			String insert = "INSERT INTO CHUNKS (FILENAME, FILEID, CHUNKNO, REPLICATION_DEGREE, DESIRED_RD)"
					+ " VALUES (" +
					"(SELECT IFNULL(MAX(FILENAME), 0) + 1 FROM CHUNKS), '"+
					fileId+"', '" +
					String.valueOf(chunkNo)+"', '" +
					String.valueOf(rd)+"', '" +
					String.valueOf(desiredRD)+"');";

			openStatement();
			statement.executeUpdate(insert);
			closeStatement();
			String path = getChunkPath(fileId, chunkNo);
			return path;
		}
		catch(SQLException e){
			System.err.println("DBInteraction - saveChunk: " + e.getErrorCode());
			System.err.println("DBInteraction - saveChunk: " + e.getMessage());
			System.err.println("DBInteraction - saveChunk: " + e.getSQLState());
			closeStatement();
		}
		return null;
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @return 
	 */
	public Chunk getChunk(String fileId, int chunkNo){

		Chunk chunk =null;
		String query = "SELECT * FROM CHUNKS" 
				+" WHERE FILEID= '" + fileId 
				+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";
		openStatement();
		try{
			ResultSet ans = statement.executeQuery(query);

			if(!ans.isAfterLast()){
				int replicationDegree = Integer.parseInt((ans.getString("REPLICATION_DEGREE")));
				int drd = Integer.parseInt(ans.getString("DESIRED_RD"));
				/*
				 * close statement before doing another access to the db by the
				 * getChunkPath() call, or we'll get a locked db.
				 */
				closeStatement();

				chunk = new Chunk(fileId, chunkNo);
				chunk.getDataFromFS(getChunkPath(fileId,chunkNo));	
				chunk.replicationDegree = replicationDegree;
				chunk.setDesiredRD(drd);
			}
			else{
				closeStatement();
			}
		}
		catch(Exception e){
			System.err.println("DBInteraction - getChunk: " + e.getMessage());
			closeStatement();
		}
		if(chunk!=null)
			chunk.replicationDegree = getChunkRepDegree(chunk.getFileId(), chunk.getChunkNo());
		return chunk;
	}

	/**
	 * Returns a chunk array with all backuped chunks
	 *
	 * @return
	 */
	public ArrayList<Chunk> getAllChunks() {
		ArrayList<Chunk> storedChunks = new ArrayList<>();
		try {


			//Execute query
			String query = "SELECT * FROM CHUNKS;";
			openStatement();
			ResultSet ans = statement.executeQuery(query);

			while (ans.next()) {

				//Parse values from db
				int chunkNo = Integer.parseInt((ans.getString("CHUNKNO")));
				int actualDegree = Integer.parseInt((ans.getString("REPLICATION_DEGREE")));
				int desiredDegree = Integer.parseInt((ans.getString("DESIRED_RD")));

				//Create chunk
				Chunk chunk = new Chunk(ans.getString("FILEID"), chunkNo);
				chunk.replicationDegree = actualDegree;
				chunk.setDesiredRD(desiredDegree);

				//Add to arraylist
				storedChunks.add(chunk);
			}

			closeStatement();
			return storedChunks;

		}  catch (Exception e) {
			System.err.println("DBInteraction - getAllChunks: " + e.getMessage());
			closeStatement();
		}

		return storedChunks;
	}

	/**
	 * 
	 * @param chunk a chunk to remove from database
	 */
	public void removeChunk(Chunk chunk){
		removeChunk(chunk.getFileId(), chunk.getChunkNo());
	}

	/**
	 * removes a chunk from the database
	 * @param fileId
	 * @param chunkNo
	 */
	public void removeChunk(String fileId, int chunkNo){
		//TODO test it
		try{		

			String deleteC = "DELETE FROM CHUNKS WHERE "
					+ "FILEID='"+ fileId
					+ "' AND CHUNKNO="+ chunkNo + ";";
			openStatement();
			statement.executeUpdate(deleteC);


		} catch(SQLException e){
			System.err.println("DBInteraction - removeChunk: " + e.getErrorCode());
			System.err.println("DBInteraction - removeChunk: " + e.getMessage());
			System.err.println("DBInteraction - removeChunk: " + e.getSQLState());
		}

		closeStatement();
	}

	/**
	 * 
	 * @param chunk
	 * @return 
	 */
	public boolean saveChunkDegree(Chunk chunk){

		return saveChunkDegree(chunk.fileId, chunk.chunkNo, chunk.replicationDegree);
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @param replicationDegree
	 * @return 
	 */
	public boolean saveChunkDegree(String fileId, int chunkNo, int replicationDegree){
		//TODO test it
		try{		


			String update = "UPDATE CHUNKS SET REPLICATION_DEGREE='" 
					+ String.valueOf(replicationDegree)
					+ "' WHERE FILEID='" + fileId
					+ "' AND CHUNKNO='" + String.valueOf(chunkNo)
					+ "';";
			openStatement();
			statement.executeUpdate(update);
			closeStatement();
			return true;
		}
		catch(Exception e){
			System.err.println("DBInteraction - saveChunkDegree: " + e.getMessage());
			closeStatement();
		}
		return false;
	}

	/**
	 * 
	 * @param chunk
	 * @return 
	 */
	public int getChunkRepDegree(Chunk chunk){

		return getChunkRepDegree(chunk.fileId, chunk.chunkNo);
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @return 
	 */
//	public int getChunkRepDegree(String fileId, int chunkNo){
//		//TODO test it
//		int chunkRD = -1;
//		try{		
//
//
//			String query = "SELECT REPLICATION_DEGREE FROM '"
//					+ CHUNKS_TABLE 
//					+"' WHERE FILEID= '" + fileId 
//					+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";
//
//			openStatement();
//			ResultSet ans = statement.executeQuery(query);
//
//			String replicationDegree = ans.getString("REPLICATION_DEGREE");
//			chunkRD = Integer.parseInt(replicationDegree);		
//
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - getChunkRepDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - getChunkRepDegree: " + e.getMessage());
//			System.err.println("DBInteraction - getChunkRepDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - getChunkRepDegree: " + e.getMessage());
//		}
//		closeStatement();
//		return chunkRD;
//	}

	
	public int getChunkRepDegree(String fileId, int chunkNo){
		
		int rd = 0;
		String query = "SELECT COUNT(*) AS TOTAL FROM CHUNKSPEERS WHERE"+
				" FILEID = '"+ fileId + "' AND CHUNKNO='"+ chunkNo +"';";
		
		openStatement();
		try {
			ResultSet ans = statement.executeQuery(query);
			while(ans.next())
				rd=ans.getInt("TOTAL");
		} catch (SQLException e) {
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getErrorCode());
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getMessage());
			System.err.println("DBInteraction - getFileChunkDegree: " + e.getSQLState());
		}
		closeStatement();
		return rd;
	}
	
	
	/**
	 * 
	 * @param chunk
	 * @return 
	 */
	public int getChunkDesiredRP(Chunk chunk){

		return getChunkDesiredRP(chunk.getFileId(), chunk.getChunkNo());
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @return 
	 */
	public int getChunkDesiredRP(String fileId, int chunkNo){
		//TODO test it

		int desiredRD = -1;
		try{		


			String query = "SELECT DESIRED_RD FROM '"
					+ CHUNKS_TABLE 
					+"' WHERE FILEID= '" + fileId 
					+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";

			openStatement();
			ResultSet ans = statement.executeQuery(query);
			String desiredRp = ans.getString("DESIRED_RD");
			desiredRD = Integer.parseInt(desiredRp);		

		}
		catch(SQLException e){
			System.err.println("DBInteraction - getChunkDesiredRP: " + e.getErrorCode());
			System.err.println("DBInteraction - getChunkDesiredRP: " + e.getMessage());
			System.err.println("DBInteraction - getChunkDesiredRP: " + e.getSQLState());
		}
		catch(Exception e){
			System.err.println("DBInteraction - getChunkDesiredRP: " + e.getMessage());
		}
		closeStatement();
		return desiredRD;
	}

	/**
	 * 
//	 * @param chunk 
//	 */
//	public void increaseChunkDegree(Chunk chunk){
//		//TODO test it
//		try{		
//
//
//			String update = "UPDATE CHUNKS SET REPLICATION_DEGREE='" 
//					+ String.valueOf(chunk.getReplicationDegree()+1)
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - increaseChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - increaseChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - increaseChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - increaseChunkDegree: " + e.getMessage());
//		}
//		closeStatement();
//	}
//
//	/**
//	 * 
//	 * @param chunk 
//	 */
//	public void decreaseChunkDegree(Chunk chunk){
//		//TODO test it
//		try{		
//
//
//			String update = "UPDATE CHUNKS SET REPLICATION_DEGREE='" 
//					+ String.valueOf(chunk.getReplicationDegree()-1)
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - decreaseChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - decreaseChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - decreaseChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - decreaseChunkDegree: " + e.getMessage());
//		}
//		closeStatement();
//	}
//
//	/**
//	 * 
//	 * @param chunk 
//	 */
//	public void resetChunkDegree(Chunk chunk){
		//TODO test it
//		try{		
//
//
//			String update = "UPDATE CHUNKS SET REPLICATION_DEGREE= 0'" 
//					+ "' WHERE FILEID='" + chunk.getFileId()
//					+ "' AND CHUNKNO='" + String.valueOf(chunk.getChunkNo())
//					+ "';";
//			openStatement();
//			statement.executeUpdate(update);
//		}
//		catch(SQLException e){
//			System.err.println("DBInteraction - resetChunkDegree: " + e.getErrorCode());
//			System.err.println("DBInteraction - resetChunkDegree: " + e.getMessage());
//			System.err.println("DBInteraction - resetChunkDegree: " + e.getSQLState());
//		}
//		catch(Exception e){
//			System.err.println("DBInteraction - resetChunkDegree: " + e.getMessage());
//		}
//		closeStatement();
//	}

	/**
	 * 
	 * @param chunk
	 * @return 
	 */
	public String getChunkPath(Chunk chunk){

		return getChunkPath(chunk.getFileId(), chunk.getChunkNo());
	}

	/**
	 * 
	 * @param fileId
	 * @param chunkNo
	 * @return 
	 */
	public String getChunkPath(String fileId, int chunkNo){
		//TODO test it
		String path = null;
		String query = "SELECT * FROM CHUNKS" 
				+" WHERE FILEID= '" + fileId 
				+"' AND CHUNKNO= '" +String.valueOf(chunkNo)+"';";

		openStatement();
		try{

			ResultSet ans = statement.executeQuery(query);
			int fileName = ans.getInt("FILENAME");
			path = Chunk.CHUNKS_PATH + String.valueOf(fileName) + ".data";
			closeStatement();

		}
		catch(Exception e){
			System.err.println("DBInteraction - getChunkPath: " + e.getMessage());
			closeStatement();
		}
		return path;
	}

	/**
	 * 
	 * @param fileId 
	 */
	public void deleteChunks(String fileId){
		//TODO test it
		try{		
			
			String filesPath = "SELECT FILENAME FROM CHUNKS WHERE FILEID='"
					+fileId+"';";
			String deleteFCs = "DELETE from CHUNKS where FILEID='"
					+ fileId + "';";
			openStatement();
			
			ResultSet ans = statement.executeQuery(filesPath);
			
			while(ans.next()){
				int fileName = ans.getInt("FILENAME");
				String path = Chunk.CHUNKS_PATH + String.valueOf(fileName) + ".data";
				
				File chunkOnDisk = new File(path);
				if(chunkOnDisk.exists())
					chunkOnDisk.delete();			
			}
			
			statement.executeUpdate(deleteFCs);


		} catch(SQLException e){
			System.err.println("DBInteraction - deleteChunks: " + e.getErrorCode());
			System.err.println("DBInteraction - deleteChunks: " + e.getMessage());
			System.err.println("DBInteraction - deleteChunks: " + e.getSQLState());
		}
		catch(Exception e){
			System.err.println("DBInteraction - deleteChunks: " + e.getMessage());
		}
		closeStatement();
	}

	public static boolean isAvailable() {
		return available;
	}

	public static void setAvailableTrue() {
		DBInteraction.available = true;
	}
	/**
	 * only one per time is allowed to execute here
	 */
	public static synchronized void setAvailableFalse(){

		increaseInsideSynch();
		Boolean printOne = true;
		while(!isAvailable()){
			try {
				//TODO random
				if(printOne)
					System.out.println(getInsideSynch() + " inside setAvailableFalse");
				printOne = false;
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//I know it's heavy, maybe a random thread.sleep()?
			/*
			 * imagine if 2 were waiting for the unlock, assume now it's unlocked
			 * and the two by luck freed themselves from the while loop at the same time
			 * only one is allowed here, so, one will wait, and as soon as the other exits
			 * it's his time here, it means that both could be locking the dbi and both are free,
			 * this offers the guarantee that one and only one is performing statements 
			 */
		}
		decreaseInsideSynch();
		DBInteraction.available = false;
	}

	public static int getInsideSynch() {
		return insideSynchronized;
	}

	public static void increaseInsideSynch() {
		DBInteraction.insideSynchronized++;
	}
	public static void decreaseInsideSynch() {
		DBInteraction.insideSynchronized--;
	}

}
