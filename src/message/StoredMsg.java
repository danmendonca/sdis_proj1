package message;

import general.Chunk;
import general.DBInteraction;
import general.Peer;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

public class StoredMsg extends PeerMsg {  

	private static int MAX_WAIT_TIME = 400;
	private String chunkNoMsg;
	private String fromAddress;
	private int fromPort;

	public String getFromAddress() {
		return fromAddress;
	}

	public int getFromPort() {
		return fromPort;
	}

	/**
	 * Class Constructor
	 * @param rawData 
	 */
	public StoredMsg(DatagramPacket packet){
		this.connectMsg = connectionType.RECEIVE;
		this.rawData = packet.getData();

		fromPort = packet.getPort();
		fromAddress = packet.getAddress().getHostAddress();

		parseMsg(parseHeader());
	}

	/**
	 * Class Constuctor
	 *
	 * @param versionM
	 * @param chunkM
	 */
	public StoredMsg(String versionM, Chunk chunkM) {
		this.connectMsg = connectionType.SEND;
		this.typeMsg = messageType.STORED;
		this.versionMsg = versionM;
		this.fileIdMsg = chunkM.getFileId();
		this.chunkNoMsg = Integer.toString(chunkM.getChunkNo());
	}

	/**
	 * Parses and validates STORED messages
	 *
	 * @param msgHeader
	 */
	private void parseMsg(String[] msgHeader) {
		if (msgHeader.length != 4) {
			throw new IllegalArgumentException("Wrong number of message fields.");
		}

		parseVersion(msgHeader[1]);
		parseFileId(msgHeader[2]);
		parseChunkNo(msgHeader[3]);

		this.typeMsg = messageType.STORED;
		this.versionMsg = msgHeader[1];
		this.fileIdMsg = msgHeader[2];
		this.chunkNoMsg = msgHeader[3];
	}

	/**
	 * Returns message Chunk Number
	 *
	 * @return Chunk Number
	 */
	@Override
	public String getChunkNoMsg() {
		return chunkNoMsg;
	}

	/**
	 * 
	 * @return 
	 */
	@Override
	public String toString() {

		StringBuilder strMsg = new StringBuilder();
		strMsg.append(super.toString());

		//Chunk No
		strMsg.append("Chunk No: ");
		if (this.chunkNoMsg != null) {
			strMsg.append(this.chunkNoMsg);
		}
		strMsg.append(System.lineSeparator());

		return strMsg.toString();
	}

	/**
	 * Returns Datagram packet with message data
	 *
	 * @param packetIP
	 * @param packetPort
	 * @return
	 */
	public DatagramPacket createPacket(InetAddress packetIP, int packetPort) {
		String cr = Character.toString((char) 13);
		String lf = Character.toString((char) 10);

		StringBuilder strBuild = new StringBuilder();

		//Type
		strBuild.append(this.typeMsg).append(" ");

		//Version
		strBuild.append(this.versionMsg).append(" ");

		//File ID
		strBuild.append(this.fileIdMsg);

		//Chunk No
		strBuild.append(" ");
		strBuild.append(this.chunkNoMsg);

		//CRLF CRLF
		strBuild.append(cr).append(lf).append(cr).append(lf);

		String packetStr = strBuild.toString();
		byte[] packetBuf;
		packetBuf = packetStr.getBytes();

		DatagramPacket packet = new DatagramPacket(packetBuf, packetBuf.length, packetIP, packetPort);
		return packet;
	}

	/* (non-Javadoc)
	 * @see message.PeerMsg#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(this.connectMsg == connectionType.SEND)
			sendStored();
		else
			receiveStored();
	}

	/**
	 * checks if the store chunk is ours or is a chunk that we backed up
	 * for someone. If it is, increases its Replication_Degree on the
	 * database, if it's not, does nothing
	 */
	private void receiveStored(){

		//TODO test it
		parseMsg(parseHeader());

		DBInteraction dbi = DBInteraction.getInstance();
		Chunk chunk = dbi.getFileChunk(this.getFileIdMsg(), Integer.parseInt( this.getChunkNoMsg()));

		//it's for a file we own
		if(chunk!=null){
			//chek if it is not a stored pair ip-port associated with this chunk
			if(dbi.isFileChunkPeer(fromAddress, fromPort, chunk.getFileId(), chunk.getChunkNo()))
				return;
			
			dbi.insertFileChunkPeer(fromAddress, fromPort, chunk.getFileId(), chunk.getChunkNo());
			return;
		}
		else{   //we may or may not have it

			chunk = dbi.getChunk(this.getFileIdMsg(), Integer.parseInt( this.getChunkNoMsg()));

			//we have it
			if(chunk != null){
				if(dbi.isChunkPeer(fromAddress, fromPort, chunk.getFileId(), chunk.getChunkNo()))
					return;
				
				dbi.insertChunkPeer(fromAddress, fromPort, chunk.getFileId(), chunk.getChunkNo());
			}
		}
	}

	/**
	 * creates a datagram with the data[] following the application STORE protocol
	 * anouncing other peers that we backed up the chunk
	 */
	private void sendStored(){
		//TODO test it
		try{
			Peer peer = Peer.getInstace();
			DatagramPacket packet = createPacket(peer.getMcInet(), peer.getMcPort());


			Random rand = new Random();
			Thread.sleep(rand.nextInt(MAX_WAIT_TIME));

			
			peer.sendMsgs(packet);
			System.out.println(this.typeMsg + " message sent.");

		}
		catch(UnknownHostException | InterruptedException e){
			System.err.println("StoredMsg - sendStored: " + e.getMessage());
		}
	}


}
