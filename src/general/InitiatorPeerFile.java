package general;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;

public class InitiatorPeerFile {

	final public static int DATASIZE = 64000;
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private String fileName;
	private String fileId;
	private int nrChunks, desiredRD;

        /**
         * 
         * @param fileName
         * @param desiredRD
         * @throws Exception 
         */
	public InitiatorPeerFile(String fileName, int desiredRD) throws Exception{
		// test it

		this.fileName = fileName;
		this.desiredRD = desiredRD;

		try {		
			File file = new File(fileName);
			if(file == null || !file.exists() || !file.isFile() || !file.canRead())
				throw new Exception("Unable to open file.");

			String sha256 = file.getName() + file.lastModified()+ file.length();

			MessageDigest msgDgst = MessageDigest.getInstance("SHA-256");

			msgDgst.update(sha256.getBytes());		
			setFileId(bytesToHex(msgDgst.digest()));

			//TODO this might not work due to overflow
			//best way is to increment as long as you process file
			int ratioSizeMaxSize = (int)file.length()/DATASIZE;
			this.nrChunks = ratioSizeMaxSize;

			if((file.length()%DATASIZE) > 0)
				this.nrChunks++;

		} catch (FileNotFoundException e) {
			System.err.println("InitiatorPeerFile - InitiatorPeerFile: " + e.getMessage());
		}

	}

        /**
         * 
         * @param fileName
         * @param desiredRD
         * @param fileId
         * @param nrChunks 
         */
	public InitiatorPeerFile(String fileName, int desiredRD, String fileId, int nrChunks){
		this.fileName= fileName;
		this.fileId = fileId;
		this.desiredRD = desiredRD;
		this.nrChunks = nrChunks;
	}

        /**
         * 
         * @throws Exception 
         */
	public void saveInitiatorPeerFile() throws Exception{
		DBInteraction dbi = DBInteraction.getInstance();
		dbi.saveFile(getFileName(), getFileId(), getNrChunks(), getdesiredRD());
	}

        /**
         * 
         * @return 
         */
	public String getFileName() {

		return fileName;
	}

        /**
         * 
         * @param fileName 
         */
	public void setFileName(String fileName) {

		this.fileName = fileName;
	}
        
        /**
         * 
         * @return 
         */
	public String getFileId() {

		return fileId;
	}

        /**
         * 
         * @param fileId 
         */
	private void setFileId(String fileId) {

		this.fileId = fileId;
	}

	/**
	 * 
	 * @return the file nr of chunks
	 */
	public int getNrChunks() {

		return nrChunks;
	}

        /**
         * 
         * @param nrChunks 
         */
	public void setNrChunks(int nrChunks) {

		this.nrChunks = nrChunks;
	}

	/**
	 * 
	 * @return desired replication degree
	 */
	public int getdesiredRD() {

		return desiredRD;
	}


	/**
	 * 
	 * @param desiredRD
	 */
	public void setReplicationDegree(int desiredRD) {

		this.desiredRD = desiredRD;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		String fileNameStr = "FileName: "+this.fileName+'\n';
		String fileIdStr = "FileId: "+ this.fileId + '\n';
		String nrChunksStr = "Nr. of chunks: " + this.nrChunks + '\n';
		String repDegreeStr = "RepDegree: " + this.desiredRD + '\n';
		return fileNameStr + fileIdStr + nrChunksStr + repDegreeStr;
	}

	/**
	 * 
	 * @param bytes the sha-256 encoded byte[]
	 * @return a string in hex format
	 */
	public static String bytesToHex(byte[] bytes) {

		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
        
        /**
         * 
         * @param s
         * @return 
         */
	public static byte[] hexToBytes(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

	/**
	 * @param chunkNo the number of the chunk you want the data for
	 * @return a chunk of the file that matches the chunkNo
	 * @throws Exception if there's a problem opening the file
	 */
	public byte[] getPartialData(int chunkNo) throws Exception{
		//TODO test it
		byte[] chunkData = null;

		//if it gets here, then file must exist, was verified in constructor
		File file = new File(this.fileName);
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		
		if(chunkNo == this.nrChunks)
			chunkData = new byte[(int )(file.length()%DATASIZE)];
		else
			chunkData = new byte[DATASIZE];

		//chunkNo starts at 1
		int offset = (chunkNo - 1) * DATASIZE;
		raf.seek(offset);
		raf.read(chunkData);		
		raf.close();

		return chunkData;
	}
	
        /**
         * 
         * @param path
         * @param chunk 
         */
	public void saveChunkToRestore(String path, Chunk chunk){
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(path + chunk.chunkNo + ".restore");
			fos.write(chunk.getData());
			fos.close();
		} catch (IOException e) {
			System.err.println("Exception " + chunk.chunkNo);
			System.err.println("InitiatorPeerFile - saveChunkToRestore: " + e.getMessage());
		}
	}
	
        /**
         * 
         */
	public void restoreFile(){
		//TODO make it work with chunk.saveIntoFs()
		try {
			FileOutputStream fos = new FileOutputStream(this.fileName);
			
			for(int i = 1; i<= this.nrChunks; i++){
			
				File file = new File(Chunk.CHUNKS_RESTORE_PATH+i+".data");
				byte[] data = new byte[(int)file.length()];
				
				RandomAccessFile raf = new RandomAccessFile(file, "r");
				raf.read(data);		
				raf.close();
				
				fos.write(data);
				file.delete();
			}
			
			fos.close();
		} catch (IOException e) {
                    System.err.println("InitiatorPeerFile - restoreFile: " + e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param chunkNo
	 * @return chunk with the data set to match the chunkNo
	 * @throws Exception
	 */
	public Chunk createChunk(int chunkNo) throws Exception{
		Chunk chunk = new Chunk(getFileId(), chunkNo);
		chunk.setDesiredRD(getdesiredRD());
		chunk.setData(getPartialData(chunkNo));

		return chunk;
	}
	
	/**
	 * deletes all its related FileChunks and itself
	 * @throws Exception
	 */
	public void delete(){
		
		DBInteraction dbi = DBInteraction.getInstance();
		
		dbi.deleteFileChunks(this.getFileId());
		dbi.deleteFile(this.getFileName());
		dbi.deleteFileChunksPeers(this.getFileId());
	}
}
