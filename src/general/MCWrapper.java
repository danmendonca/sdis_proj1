/**
 * 
 */
package general;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import message.ChunkMsg;
import message.PeerMsg;
import message.PutMsg;


public class MCWrapper implements Runnable {
	
	final private int DATASIZE =  80000;
	MulticastSocket mc;
	private Boolean runChannel = true;
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		
		while(runChannel){
			byte[] data = new byte[DATASIZE];
			DatagramPacket packet = new DatagramPacket(data, DATASIZE);
			
			try {
				Peer peer = Peer.getInstace();
				mc.receive(packet);
				
				int packetPort, senderPort;
				InetAddress packetAddr ,senderAddr;
				packetPort = packet.getPort();
				senderPort = peer.getSenderPort();
				packetAddr = packet.getAddress();
				senderAddr = InetAddress.getLocalHost();
				
				
				if(packetPort == senderPort &&	packetAddr.equals(senderAddr))
					continue;
				
				int bytesRead = packet.getLength();
				PeerMsg peerMsg = PeerMsg.getInstance(packet, bytesRead);
                                System.out.println("MC: " + peerMsg.getTypeMsg() + " message received.");
				
				if((peerMsg instanceof PutMsg) || (peerMsg instanceof ChunkMsg))
					continue;
				
				Thread processDatagram = new Thread(peerMsg);
				
				processDatagram.start();
				
			} catch (IOException e) {
				System.err.println("MCWrapper - run: " + e.getMessage());
			}
			
		}
	}
	
        /**
         * 
         */
	public void closeChannel(){
		runChannel =false;
		mc.close();
	}
	
        /**
         * 
         * @param address
         * @param port
         * @throws IOException 
         */
	public MCWrapper(String address, int port) throws IOException{
		
			InetAddress addr = InetAddress.getByName(address);
			mc = new MulticastSocket(port);
			mc.joinGroup(addr);
	}
}
