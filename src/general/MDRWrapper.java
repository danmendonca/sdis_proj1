package general;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import message.PeerMsg;

/**
 * Class MDRWrapper
 *
 */
public class MDRWrapper implements Runnable {

    MulticastSocket mdr;
    final private int DATASIZE = 80000;

    private Boolean runChannel = true;
    private Boolean listenChannel = false;
    private ArrayList<Chunk> chunkList;

    /**
     * Class Constructor
     *
     * @param address
     * @param port
     * @throws IOException
     */
    public MDRWrapper(String address, int port) throws IOException {
        this.chunkList = new ArrayList<>();
        InetAddress addr = InetAddress.getByName(address);
        mdr = new MulticastSocket(port);
        mdr.joinGroup(addr);
    }

    /**
     * Ends while cycle in run() method and closes socket
     */
    public void closeChannel() {
        this.runChannel = false;
        mdr.close();
    }

    /**
     * Thread Method
     */
    @Override
    public void run() {

        while (runChannel) {
            byte[] data = new byte[DATASIZE];
            DatagramPacket packet = new DatagramPacket(data, DATASIZE);

            try {

                //Receive and process datagram packet
                mdr.receive(packet);
                int bytesRead = packet.getLength();
                PeerMsg pMsg = PeerMsg.getInstance(packet, bytesRead);
                System.out.println("MDR: " + pMsg.getTypeMsg() + " message received.");

                //Verify valid message type
                if (pMsg.getTypeMsg() == PeerMsg.messageType.CHUNK) {

                    //If we are listening to the MDR channel, than we will log the chunk arrival
                    if (this.listenChannel) {
                        chunkList.add(new Chunk(pMsg.getFileIdMsg(), Integer.parseInt(pMsg.getChunkNoMsg())));
                    }

                    //Launch CHUNK message processing
                    Thread t = new Thread(pMsg);
                    t.start();
                }

            } catch (IOException e) {
                System.err.println("MDRWrapper - run: " + e.getMessage());
            }

        }

    }

    /**
     * Starts listening to the channel and saving the received CHUNK messages in
     * the chunkList
     */
    public void startListen() {
        this.chunkList = new ArrayList<>();
        this.listenChannel = true;
    }

    /**
     * Stops listening to the channel and saving the received chunk messages in
     */
    public void stopListen() {
        this.listenChannel = false;
    }

    /**
     * Checks if we received an specified chunk
     * @param fileId
     * @param chunkNo
     * @return
     */
    public Boolean findChunck(String fileId, String chunkNo) {
        for (Chunk ch : this.chunkList) {
            if (ch.getFileId().equals(fileId) && Integer.toString(ch.getChunkNo()).equals(chunkNo)) {
                return true;
            }
        }
        return false;
    }
}
